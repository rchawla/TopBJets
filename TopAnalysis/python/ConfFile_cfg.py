import FWCore.ParameterSet.Config as cms
import FWCore.Utilities.FileUtils as FileUtils
#mylist = FileUtils.loadListFromFile ('ttbarfiles_bdecays_UL2018_AOD.txt') 
#readFiles = cms.untracked.vstring( *mylist)

process = cms.Process("Demo")

# Load the standard set of configuration modules
process.load("TrackingTools.TransientTrack.TransientTrackBuilder_cfi")
process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_38T_cff')
process.load("Configuration.Geometry.GeometryRecoDB_cff")
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Geometry.CaloEventSetup.CaloTowerConstituents_cfi")

process.load("SimGeneral.HepPDTESSource.pythiapdt_cfi")
process.printTree = cms.EDAnalyzer("ParticleTreeDrawer",
src = cms.InputTag("prunedGenParticles"),
)

process.printList = cms.EDAnalyzer("ParticleListDrawer",
    src = cms.InputTag("packedGenParticles"),
    maxEventsToPrint = cms.untracked.int32(-1),
    printVertex = cms.untracked.bool(True)
)

# Global tag
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '102X_dataRun2_v12') # data
#process.GlobalTag = GlobalTag(process.GlobalTag, '102X_upgrade2018_realistic_v20') # MC

# Message logger
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 10000

# Set input to process
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100) )
process.source = cms.Source("PoolSource",
    #fileNames = readFiles,
    #fileNames = cms.untracked.vstring('file:CF0B805F-B82E-5943-9784-B2B88425E5EE.root'),
    fileNames = cms.untracked.vstring('file:001CBBFC-42B3-4C43-A2CA-75E72C3D1A9D.root'),
    ###fileNames = cms.untracked.vstring('file:010C7041-20C6-DC44-843E-BA9614CDF079.root'),
    dropDescendantsOfDroppedBranches=cms.untracked.bool(False),
    inputCommands=cms.untracked.vstring(
            'keep *', 
            'drop recoTrackExtrasedmAssociation_muonReducedTrackExtras__RECO')
)

#define the default IDs to produce in VID
_defaultEleIDModules = ['RecoEgamma.ElectronIdentification.Identification.cutBasedElectronID_Fall17_94X_V2_cff']

from EgammaUser.EgammaPostRecoTools.EgammaPostRecoTools import setupEgammaPostRecoSeq
setupEgammaPostRecoSeq(process,
                       applyEnergyCorrections=False,
                       isMiniAOD=False,
                       era='2018-UL',
                       eleIDModules=_defaultEleIDModules,
                       runEnergyCorrections=False) 

process.demo = cms.EDAnalyzer('TopAnalysis',
    isMC = cms.untracked.bool(False),
    egmElectronID = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Fall17-94X-V2-tight"),
    genparticles = cms.InputTag("genParticles"),
    offlinePrimaryVertices = cms.InputTag('offlinePrimaryVertices'),
    ak4TrackJets = cms.InputTag("ak4TrackJets"),
    generalTracks = cms.InputTag("generalTracks"),
    muons = cms.InputTag("muons"),
    gedGsfElectrons = cms.InputTag("gedGsfElectrons")
)

process.primaryVertexFilter  = cms.EDFilter("VertexSelector",
    src = cms.InputTag('offlineSlimmedPrimaryVertices'),
    cut = cms.string('!isFake && ndof > 4.0 && position.Rho < 2.0 && abs(z) < 24'),
    filter = cms.bool(True)
)

# Tfile service
process.TFileService = cms.Service("TFileService",
    fileName = cms.string('mumu_Run2018A.root')
    #fileName = cms.string('ttbarbdecays_UL2018.root')
)

process.p = cms.Path(process.egammaPostRecoSeq+process.demo)
