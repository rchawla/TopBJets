//// -*- C++ -*-
//
// Package:    TopBJets/TopAnalysis
// Class:      TopAnalysis
// 
/**\class TopAnalysis TopAnalysis.cc TopBJets/TopAnalysis/plugins/TopAnalysis.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Ridhi Chawla
//         Created:  Thu, 19 Sep 2019 18:16:53 GMT
//
//

// system include files
#include <memory>

// user include files
#include <vector>
#include <iostream>
#include <TLorentzVector.h>
#include <TMath.h>
#include "TTree.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/Candidate/interface/Particle.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/RecoCandidate/interface/RecoCandidate.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/JetReco/interface/TrackJet.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/EgammaCandidates/interface/Electron.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/Common/interface/ValueMap.h"
#include "RecoEgamma/EgammaTools/interface/ConversionTools.h"
#include "RecoEgamma/EgammaTools/interface/EffectiveAreas.h"
#include "DataFormats/PatCandidates/interface/MET.h"

using namespace edm;
using namespace std;
using namespace reco;
using namespace TMath;

// Typedefs
typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > LV;

struct comp_muon_pt {
  bool operator()(const reco::Muon *a,const reco::Muon *b) {
    return a->pt() > b->pt();
  }
};

struct comp_electron_pt {
  bool operator()(const reco::GsfElectron *a,const reco::GsfElectron *b) {
    return a->pt() > b->pt();
  }
};

class TopAnalysis : public edm::one::EDAnalyzer<edm::one::SharedResources>  {
   public:
      explicit TopAnalysis(const edm::ParameterSet&);
      ~TopAnalysis();
      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

   private:
      virtual void beginJob() override;
      virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
      virtual void endJob() override;

      void GenAnalysis(const edm::Event&,const edm::EventSetup&,
                       edm::Handle<edm::View<reco::GenParticle> > &);
      void RecoAnalysis(const edm::Event&,const edm::EventSetup&,
                        edm::Handle<edm::View<reco::Track> > &);
      void DataAnalysis(const edm::Event&,const edm::EventSetup&,
                        edm::Handle<edm::View<reco::Muon> > &,
                        edm::Handle<edm::View<reco::GsfElectron> > &);

      template<typename TYPE> LV getLV(const TYPE&) const;
      bool isTopAncestor(const reco::Candidate*);
      bool isMuon(const reco::Muon*);
      bool dRLeptonJet(std::vector<const reco::Muon*>,const reco::TrackJet &);
      edm::Ptr<reco::Track> matchingLepton(edm::Ptr<reco::Track>&,
                                           edm::Handle<edm::View<reco::Muon> > &);
      edm::Ptr<reco::Track> matchingLepton(edm::Ptr<reco::Track>&,
                                           edm::Handle<edm::View<reco::GsfElectron> > &);
      std::vector<const reco::Track*> matchingTrack(const reco::Candidate*,
                                                    edm::Handle<edm::View<reco::Track> > &);
      std::vector<std::vector<double> > unpackTrackParameters(const reco::Candidate*,
                                                             edm::Ptr<reco::Track>&,std::string);
      std::vector<std::vector<double> > unpackTrackCovariance(const reco::Track*,
                                                             edm::Ptr<reco::Track>&,std::string);

      edm::EDGetTokenT<edm::View<reco::GenParticle> > genToken_;
      edm::EDGetTokenT<edm::View<reco::Vertex> > vertexToken_;
      edm::EDGetTokenT<edm::View<reco::TrackJet> > jetsToken_;
      edm::EDGetTokenT<edm::View<reco::Track> > generalTracksToken_;
      edm::EDGetTokenT<edm::View<reco::Muon> > muonsToken_;
      edm::EDGetTokenT<edm::View<reco::GsfElectron> > electronsToken_;
      edm::EDGetTokenT<edm::View<reco::PFMET> > metToken_;
      edm::EDGetTokenT<edm::ValueMap<bool> > electronIdToken_;

      // Global variables
      bool misMC;
      bool mumu, ee,
           emu, mue,
           zmm, zee;
      const double mk = 0.493677;
      const double mpi = 0.13957039;
      const double mp = 0.9382720813;
      const double me = 0.00051099895;
      const double mmu = 0.1056583755;
      unsigned nkpi, nkpipi, nkkpi, ndstar, npkpi;
      const reco::Vertex *_pv;
      int nlepton;
      edm::Ptr<reco::Track> nulltrack;
      std::vector<const reco::Muon*> muons;
      std::vector<const reco::GsfElectron*> electrons;
      std::vector<const reco::Candidate*> top_quarks;
      std::vector<const reco::Candidate*> bottom_hadrons;
      std::vector<const reco::Candidate*> charm_hadrons;
      std::vector<const reco::Candidate*> leptons;
      std::vector<const reco::Candidate*> dp_hadron, lepton_dp, kaon_dp, pion1_dp, pion2_dp;
      std::vector<const reco::Candidate*> d0_hadron, lepton_d0, kaon_d0, pion_d0;
      std::vector<const reco::Candidate*> dt_hadron, lepton_dt, kaon_dt, pion_dt, pionsoft_dt;
      std::vector<const reco::Candidate*> ds_hadron, lepton_ds, kaon1_ds, kaon2_ds, pion_ds;
      std::vector<const reco::Candidate*> lambda_hadron, lepton_lambda, kaon_lambda, pion_lambda, proton_lambda;
      std::vector<double> para, covmat;
      std::vector<std::vector<double> > par, covm;
      std::vector<std::vector<double> > ldp_par, kdp_par, pi1dp_par, pi2dp_par,
                                        ldp_cov, kdp_cov, pi1dp_cov, pi2dp_cov;
      std::vector<std::vector<double> > kd0_par, pid0_par, kd0_cov, pid0_cov;
      std::vector<std::vector<double> > kdt_par, pidt_par, pisdt_par, kdt_cov, pidt_cov, pisdt_cov;
      std::vector<std::vector<double> > k1ds_par, k2ds_par, pids_par, k1ds_cov, k2ds_cov, pids_cov;
      std::vector<std::vector<double> > klambda_par, pilambda_par, plambda_par,
                                        klambda_cov, pilambda_cov, plambda_cov;

      // Ntuple and branch variables
      TTree* ntuple_;
      edm::Service<TFileService> fileService;
      UInt_t run_,
             event_,
             lumi_;
      double nPVertex_,
             mVertex1_,
             mVertex2_,
             zPVertex_,
             vxTrack_,
             vyTrack_,
             dz_trackPV_,
             pfmet_,
             nTrackJet1_,
             nTrackJet2_,
             nJetPV_;
      std::vector<double> pt_gen_lepton_;
      std::vector<double> eta_gen_lepton_;
      std::vector<double> dz_gen_leptonPV_;
      std::vector<LV> p4_gen_muon_;
      std::vector<LV> p4_gen_electron_;
      std::vector<LV> p4_gen_matched_muon_;
      std::vector<LV> p4_gen_matched_electron_;
      std::vector<LV> p4_DPlus_;
      std::vector<LV> p4_DPlus_kaon_;
      std::vector<LV> p4_DPlus_pion1_;
      std::vector<LV> p4_DPlus_pion2_;
      std::vector<LV> p4_D0_;
      std::vector<LV> p4_D0_kaon_;
      std::vector<LV> p4_D0_pion_;
      std::vector<double> mdiff_Dstar_D0_;
      std::vector<LV> p4_Dstar_;
      std::vector<LV> p4_Dstar_D0_;
      std::vector<LV> p4_Dstar_D0_kaon_;
      std::vector<LV> p4_Dstar_D0_pion_;
      std::vector<LV> p4_Dstar_pionsoft_;
      std::vector<LV> p4_DsPlus_;
      std::vector<LV> p4_DsPlus_kaon1_;
      std::vector<LV> p4_DsPlus_kaon2_;
      std::vector<LV> p4_DsPlus_pion_;
      std::vector<LV> p4_LambdacPlus_;
      std::vector<LV> p4_LambdacPlus_kaon_;
      std::vector<LV> p4_LambdacPlus_pion_;
      std::vector<LV> p4_LambdacPlus_proton_;
      std::vector<double> v_charge_DPlus_kaon_;
      std::vector<double> v_charge_DPlus_pion1_;
      std::vector<double> v_charge_DPlus_pion2_;
      std::vector<double> v_charge_D0_kaon_;
      std::vector<double> v_charge_D0_pion_;
      std::vector<double> v_charge_Dstar_D0_kaon_;
      std::vector<double> v_charge_Dstar_D0_pion_;
      std::vector<double> v_charge_Dstar_pionsoft_;
      std::vector<double> v_charge_DsPlus_kaon1_;
      std::vector<double> v_charge_DsPlus_kaon2_;
      std::vector<double> v_charge_DsPlus_pion_;
      std::vector<double> v_charge_LambdacPlus_kaon_;
      std::vector<double> v_charge_LambdacPlus_pion_;
      std::vector<double> v_charge_LambdacPlus_proton_;
      std::vector<std::vector<double> > v_px_DPlus_lepton_;
      std::vector<std::vector<double> > v_py_DPlus_lepton_;
      std::vector<std::vector<double> > v_pz_DPlus_lepton_;
      std::vector<std::vector<double> > v_pt_DPlus_lepton_;
      std::vector<std::vector<double> > v_eta_DPlus_lepton_;
      std::vector<std::vector<double> > v_phi_DPlus_lepton_;
      std::vector<std::vector<double> > v_mass_DPlus_lepton_;
      std::vector<std::vector<double> > v_energy_DPlus_lepton_;
      std::vector<std::vector<double> > v_charge_DPlus_lepton_;
      std::vector<std::vector<double> > v_px_D0_lepton_;
      std::vector<std::vector<double> > v_py_D0_lepton_;
      std::vector<std::vector<double> > v_pz_D0_lepton_;
      std::vector<std::vector<double> > v_pt_D0_lepton_;
      std::vector<std::vector<double> > v_eta_D0_lepton_;
      std::vector<std::vector<double> > v_phi_D0_lepton_;
      std::vector<std::vector<double> > v_mass_D0_lepton_;
      std::vector<std::vector<double> > v_energy_D0_lepton_;
      std::vector<std::vector<double> > v_charge_D0_lepton_;
      std::vector<std::vector<double> > v_px_Dstar_lepton_;
      std::vector<std::vector<double> > v_py_Dstar_lepton_;
      std::vector<std::vector<double> > v_pz_Dstar_lepton_;
      std::vector<std::vector<double> > v_pt_Dstar_lepton_;
      std::vector<std::vector<double> > v_eta_Dstar_lepton_;
      std::vector<std::vector<double> > v_phi_Dstar_lepton_;
      std::vector<std::vector<double> > v_mass_Dstar_lepton_;
      std::vector<std::vector<double> > v_energy_Dstar_lepton_;
      std::vector<std::vector<double> > v_charge_Dstar_lepton_;
      std::vector<std::vector<double> > v_px_DsPlus_lepton_;
      std::vector<std::vector<double> > v_py_DsPlus_lepton_;
      std::vector<std::vector<double> > v_pz_DsPlus_lepton_;
      std::vector<std::vector<double> > v_pt_DsPlus_lepton_;
      std::vector<std::vector<double> > v_eta_DsPlus_lepton_;
      std::vector<std::vector<double> > v_phi_DsPlus_lepton_;
      std::vector<std::vector<double> > v_mass_DsPlus_lepton_;
      std::vector<std::vector<double> > v_energy_DsPlus_lepton_;
      std::vector<std::vector<double> > v_charge_DsPlus_lepton_;
      std::vector<std::vector<double> > v_px_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_py_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_pz_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_pt_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_eta_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_phi_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_mass_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_energy_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_charge_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_costheta_LambdacPlus_lepton_;
      std::vector<std::vector<double> > v_trkpara_DPlus_kaon_;
      std::vector<std::vector<double> > v_trkpara_DPlus_pion1_;
      std::vector<std::vector<double> > v_trkpara_DPlus_pion2_;
      std::vector<std::vector<double> > v_trkcovm_DPlus_kaon_;
      std::vector<std::vector<double> > v_trkcovm_DPlus_pion1_;
      std::vector<std::vector<double> > v_trkcovm_DPlus_pion2_;
      std::vector<std::vector<double> > v_trkpara_D0_kaon_;
      std::vector<std::vector<double> > v_trkpara_D0_pion_;
      std::vector<std::vector<double> > v_trkcovm_D0_kaon_;
      std::vector<std::vector<double> > v_trkcovm_D0_pion_;
      std::vector<std::vector<double> > v_trkpara_Dstar_D0_kaon_;
      std::vector<std::vector<double> > v_trkpara_Dstar_D0_pion_;
      std::vector<std::vector<double> > v_trkpara_Dstar_pionsoft_;
      std::vector<std::vector<double> > v_trkcovm_Dstar_D0_kaon_;
      std::vector<std::vector<double> > v_trkcovm_Dstar_D0_pion_;
      std::vector<std::vector<double> > v_trkcovm_Dstar_pionsoft_;
      std::vector<std::vector<double> > v_trkpara_DsPlus_kaon1_;
      std::vector<std::vector<double> > v_trkpara_DsPlus_kaon2_;
      std::vector<std::vector<double> > v_trkpara_DsPlus_pion_;
      std::vector<std::vector<double> > v_trkcovm_DsPlus_kaon1_;
      std::vector<std::vector<double> > v_trkcovm_DsPlus_kaon2_;
      std::vector<std::vector<double> > v_trkcovm_DsPlus_pion_;
      std::vector<std::vector<double> > v_trkpara_LambdacPlus_kaon_;
      std::vector<std::vector<double> > v_trkpara_LambdacPlus_pion_;
      std::vector<std::vector<double> > v_trkpara_LambdacPlus_proton_; 
      std::vector<std::vector<double> > v_trkcovm_LambdacPlus_kaon_;
      std::vector<std::vector<double> > v_trkcovm_LambdacPlus_pion_;
      std::vector<std::vector<double> > v_trkcovm_LambdacPlus_proton_;
      std::vector<double> v_pv_lxy_DPlus_;
      std::vector<double> v_pv_lxy_D0_;
      std::vector<double> v_pv_lxy_Dstar_D0_;
      std::vector<double> v_pv_lxy_DsPlus_;
      std::vector<double> v_pv_lxy_LambdacPlus_;
};

TopAnalysis::TopAnalysis(const edm::ParameterSet& iConfig) :
  genToken_(consumes<edm::View<reco::GenParticle> >(iConfig.getParameter<edm::InputTag>("genparticles"))),
  vertexToken_(consumes<edm::View<reco::Vertex> >(iConfig.getParameter<edm::InputTag>("offlinePrimaryVertices"))),
  jetsToken_(consumes<edm::View<reco::TrackJet> >(iConfig.getParameter<edm::InputTag>("ak4TrackJets"))),
  generalTracksToken_(consumes<edm::View<reco::Track> >(iConfig.getParameter<edm::InputTag>("generalTracks"))),
  muonsToken_(consumes<edm::View<reco::Muon> >(iConfig.getParameter<edm::InputTag>("muons"))),
  electronsToken_(consumes<edm::View<reco::GsfElectron> >(iConfig.getParameter<edm::InputTag>("gedGsfElectrons"))),
  metToken_(consumes<edm::View<reco::PFMET> >(iConfig.getParameter<edm::InputTag>("pfMet"))),
  electronIdToken_(consumes<edm::ValueMap<bool> >(iConfig.getParameter<edm::InputTag>("egmElectronID")))
{
  misMC = iConfig.getUntrackedParameter<bool>("isMC");

  ntuple_ = fileService->make<TTree>("Ntuple", "Ntuple");
  ntuple_->Branch("event", &event_);
  ntuple_->Branch("nPVertex", &nPVertex_);
  ntuple_->Branch("mVertex1", &mVertex1_);
  ntuple_->Branch("mVertex2", &mVertex2_);
  ntuple_->Branch("zPVertex", &zPVertex_);
  ntuple_->Branch("vxTrack", &vxTrack_);
  ntuple_->Branch("vyTrack", &vyTrack_);
  ntuple_->Branch("dz_trackPV", &dz_trackPV_);
  ntuple_->Branch("pfmet", &pfmet_);
  ntuple_->Branch("nTrackJet1", &nTrackJet1_);
  ntuple_->Branch("nTrackJet2", &nTrackJet2_);
  ntuple_->Branch("nJetPV", &nJetPV_);
  ntuple_->Branch("mumu", &mumu);
  ntuple_->Branch("ee", &ee);
  ntuple_->Branch("emu", &emu);
  ntuple_->Branch("mue", &mue);
  ntuple_->Branch("zmm", &zmm);
  ntuple_->Branch("zee", &zee);
  ntuple_->Branch("pt_gen_lepton", &pt_gen_lepton_);
  ntuple_->Branch("eta_gen_lepton", &eta_gen_lepton_);
  ntuple_->Branch("dz_gen_leptonPV", &dz_gen_leptonPV_);
  ntuple_->Branch("p4_gen_muon", &p4_gen_muon_);
  ntuple_->Branch("p4_gen_electron", &p4_gen_electron_);
  ntuple_->Branch("p4_gen_matched_muon", &p4_gen_matched_muon_);
  ntuple_->Branch("p4_gen_matched_electron", &p4_gen_matched_electron_);
  ntuple_->Branch("p4_DPlus", &p4_DPlus_);
  ntuple_->Branch("p4_DPlus_kaon", &p4_DPlus_kaon_);
  ntuple_->Branch("p4_DPlus_pion1", &p4_DPlus_pion1_);
  ntuple_->Branch("p4_DPlus_pion2", &p4_DPlus_pion2_);
  ntuple_->Branch("v_charge_DPlus_kaon", &v_charge_DPlus_kaon_);
  ntuple_->Branch("v_charge_DPlus_pion1", &v_charge_DPlus_pion1_);
  ntuple_->Branch("v_charge_DPlus_pion2", &v_charge_DPlus_pion2_);
  ntuple_->Branch("v_px_DPlus_lepton", &v_px_DPlus_lepton_);
  ntuple_->Branch("v_py_DPlus_lepton", &v_py_DPlus_lepton_);
  ntuple_->Branch("v_pz_DPlus_lepton", &v_pz_DPlus_lepton_);
  ntuple_->Branch("v_pt_DPlus_lepton", &v_pt_DPlus_lepton_);
  ntuple_->Branch("v_eta_DPlus_lepton", &v_eta_DPlus_lepton_);
  ntuple_->Branch("v_phi_DPlus_lepton", &v_phi_DPlus_lepton_);
  ntuple_->Branch("v_mass_DPlus_lepton", &v_mass_DPlus_lepton_);
  ntuple_->Branch("v_energy_DPlus_lepton", &v_energy_DPlus_lepton_);
  ntuple_->Branch("v_charge_DPlus_lepton", &v_charge_DPlus_lepton_);
  ntuple_->Branch("v_trkpara_DPlus_kaon", &v_trkpara_DPlus_kaon_);
  ntuple_->Branch("v_trkpara_DPlus_pion1", &v_trkpara_DPlus_pion1_);
  ntuple_->Branch("v_trkpara_DPlus_pion2", &v_trkpara_DPlus_pion2_);
  ntuple_->Branch("v_trkcovm_DPlus_kaon", &v_trkcovm_DPlus_kaon_);
  ntuple_->Branch("v_trkcovm_DPlus_pion1", &v_trkcovm_DPlus_pion1_);
  ntuple_->Branch("v_trkcovm_DPlus_pion2", &v_trkcovm_DPlus_pion2_);
  ntuple_->Branch("p4_D0", &p4_D0_);
  ntuple_->Branch("p4_D0_kaon", &p4_D0_kaon_);
  ntuple_->Branch("p4_D0_pion", &p4_D0_pion_);
  ntuple_->Branch("v_charge_D0_kaon", &v_charge_D0_kaon_);
  ntuple_->Branch("v_charge_D0_pion", &v_charge_D0_pion_);
  ntuple_->Branch("v_px_D0_lepton", &v_px_D0_lepton_);
  ntuple_->Branch("v_py_D0_lepton", &v_py_D0_lepton_);
  ntuple_->Branch("v_pz_D0_lepton", &v_pz_D0_lepton_);
  ntuple_->Branch("v_pt_D0_lepton", &v_pt_D0_lepton_);
  ntuple_->Branch("v_eta_D0_lepton", &v_eta_D0_lepton_);
  ntuple_->Branch("v_phi_D0_lepton", &v_phi_D0_lepton_);
  ntuple_->Branch("v_mass_D0_lepton", &v_mass_D0_lepton_);
  ntuple_->Branch("v_energy_D0_lepton", &v_energy_D0_lepton_);
  ntuple_->Branch("v_charge_D0_lepton", &v_charge_D0_lepton_);
  ntuple_->Branch("v_trkpara_D0_kaon", &v_trkpara_D0_kaon_);
  ntuple_->Branch("v_trkpara_D0_pion", &v_trkpara_D0_pion_);
  ntuple_->Branch("v_trkcovm_D0_kaon", &v_trkcovm_D0_kaon_);
  ntuple_->Branch("v_trkcovm_D0_pion", &v_trkcovm_D0_pion_);
  ntuple_->Branch("mdiff_Dstar_D0", &mdiff_Dstar_D0_);
  ntuple_->Branch("p4_Dstar", &p4_Dstar_);
  ntuple_->Branch("p4_Dstar_D0", &p4_Dstar_D0_);
  ntuple_->Branch("p4_Dstar_D0_kaon", &p4_Dstar_D0_kaon_);
  ntuple_->Branch("p4_Dstar_D0_pion", &p4_Dstar_D0_pion_);
  ntuple_->Branch("p4_Dstar_pionsoft", &p4_Dstar_pionsoft_);
  ntuple_->Branch("v_charge_Dstar_D0_kaon", &v_charge_Dstar_D0_kaon_);
  ntuple_->Branch("v_charge_Dstar_D0_pion", &v_charge_Dstar_D0_pion_);
  ntuple_->Branch("v_charge_Dstar_pionsoft", &v_charge_Dstar_pionsoft_);
  ntuple_->Branch("v_px_Dstar_lepton", &v_px_Dstar_lepton_);
  ntuple_->Branch("v_py_Dstar_lepton", &v_py_Dstar_lepton_);
  ntuple_->Branch("v_pz_Dstar_lepton", &v_pz_Dstar_lepton_);
  ntuple_->Branch("v_pt_Dstar_lepton", &v_pt_Dstar_lepton_);
  ntuple_->Branch("v_eta_Dstar_lepton", &v_eta_Dstar_lepton_);
  ntuple_->Branch("v_phi_Dstar_lepton", &v_phi_Dstar_lepton_);
  ntuple_->Branch("v_mass_Dstar_lepton", &v_mass_Dstar_lepton_);
  ntuple_->Branch("v_energy_Dstar_lepton", &v_energy_Dstar_lepton_);
  ntuple_->Branch("v_charge_Dstar_lepton", &v_charge_Dstar_lepton_);
  ntuple_->Branch("v_trkpara_Dstar_D0_kaon", &v_trkpara_Dstar_D0_kaon_);
  ntuple_->Branch("v_trkpara_Dstar_D0_pion", &v_trkpara_Dstar_D0_pion_);
  ntuple_->Branch("v_trkpara_Dstar_pionsoft", &v_trkpara_Dstar_pionsoft_);
  ntuple_->Branch("v_trkcovm_Dstar_D0_kaon", &v_trkcovm_Dstar_D0_kaon_);
  ntuple_->Branch("v_trkcovm_Dstar_D0_pion", &v_trkcovm_Dstar_D0_pion_);
  ntuple_->Branch("v_trkcovm_Dstar_pionsoft", &v_trkcovm_Dstar_pionsoft_);
  ntuple_->Branch("p4_DsPlus", &p4_DsPlus_);
  ntuple_->Branch("p4_DsPlus_kaon1", &p4_DsPlus_kaon1_);
  ntuple_->Branch("p4_DsPlus_kaon2", &p4_DsPlus_kaon2_);
  ntuple_->Branch("p4_DsPlus_pion", &p4_DsPlus_pion_);
  ntuple_->Branch("v_charge_DsPlus_kaon1", &v_charge_DsPlus_kaon1_);
  ntuple_->Branch("v_charge_DsPlus_kaon2", &v_charge_DsPlus_kaon2_);
  ntuple_->Branch("v_charge_DsPlus_pion", &v_charge_DsPlus_pion_);
  ntuple_->Branch("v_px_DsPlus_lepton", &v_px_DsPlus_lepton_);
  ntuple_->Branch("v_py_DsPlus_lepton", &v_py_DsPlus_lepton_);
  ntuple_->Branch("v_pz_DsPlus_lepton", &v_pz_DsPlus_lepton_);
  ntuple_->Branch("v_pt_DsPlus_lepton", &v_pt_DsPlus_lepton_);
  ntuple_->Branch("v_eta_DsPlus_lepton", &v_eta_DsPlus_lepton_);
  ntuple_->Branch("v_phi_DsPlus_lepton", &v_phi_DsPlus_lepton_);
  ntuple_->Branch("v_mass_DsPlus_lepton", &v_mass_DsPlus_lepton_);
  ntuple_->Branch("v_energy_DsPlus_lepton", &v_energy_DsPlus_lepton_);
  ntuple_->Branch("v_charge_DsPlus_lepton", &v_charge_DsPlus_lepton_);
  ntuple_->Branch("v_trkpara_DsPlus_kaon1", &v_trkpara_DsPlus_kaon1_);
  ntuple_->Branch("v_trkpara_DsPlus_kaon2", &v_trkpara_DsPlus_kaon2_);
  ntuple_->Branch("v_trkpara_DsPlus_pion", &v_trkpara_DsPlus_pion_);
  ntuple_->Branch("v_trkcovm_DsPlus_kaon1", &v_trkcovm_DsPlus_kaon1_);
  ntuple_->Branch("v_trkcovm_DsPlus_kaon2", &v_trkcovm_DsPlus_kaon2_);
  ntuple_->Branch("v_trkcovm_DsPlus_pion", &v_trkcovm_DsPlus_pion_);
  ntuple_->Branch("p4_LambdacPlus", &p4_LambdacPlus_);
  ntuple_->Branch("p4_LambdacPlus_kaon", &p4_LambdacPlus_kaon_);
  ntuple_->Branch("p4_LambdacPlus_pion", &p4_LambdacPlus_pion_);
  ntuple_->Branch("p4_LambdacPlus_proton", &p4_LambdacPlus_proton_);
  ntuple_->Branch("v_charge_LambdacPlus_kaon", &v_charge_LambdacPlus_kaon_);
  ntuple_->Branch("v_charge_LambdacPlus_pion", &v_charge_LambdacPlus_pion_);
  ntuple_->Branch("v_charge_LambdacPlus_proton", &v_charge_LambdacPlus_proton_);
  ntuple_->Branch("v_px_LambdacPlus_lepton", &v_px_LambdacPlus_lepton_);
  ntuple_->Branch("v_py_LambdacPlus_lepton", &v_py_LambdacPlus_lepton_);
  ntuple_->Branch("v_pz_LambdacPlus_lepton", &v_pz_LambdacPlus_lepton_);
  ntuple_->Branch("v_pt_LambdacPlus_lepton", &v_pt_LambdacPlus_lepton_);
  ntuple_->Branch("v_eta_LambdacPlus_lepton", &v_eta_LambdacPlus_lepton_);
  ntuple_->Branch("v_phi_LambdacPlus_lepton", &v_phi_LambdacPlus_lepton_);
  ntuple_->Branch("v_mass_LambdacPlus_lepton", &v_mass_LambdacPlus_lepton_);
  ntuple_->Branch("v_energy_LambdacPlus_lepton", &v_energy_LambdacPlus_lepton_);
  ntuple_->Branch("v_charge_LambdacPlus_lepton", &v_charge_LambdacPlus_lepton_);
  ntuple_->Branch("v_costheta_LambdacPlus_lepton", &v_costheta_LambdacPlus_lepton_);
  ntuple_->Branch("v_trkpara_LambdacPlus_kaon", &v_trkpara_LambdacPlus_kaon_);
  ntuple_->Branch("v_trkpara_LambdacPlus_pion", &v_trkpara_LambdacPlus_pion_);
  ntuple_->Branch("v_trkpara_LambdacPlus_proton", &v_trkpara_LambdacPlus_proton_);
  ntuple_->Branch("v_trkcovm_LambdacPlus_kaon", &v_trkcovm_LambdacPlus_kaon_);
  ntuple_->Branch("v_trkcovm_LambdacPlus_pion", &v_trkcovm_LambdacPlus_pion_);
  ntuple_->Branch("v_trkcovm_LambdacPlus_proton", &v_trkcovm_LambdacPlus_proton_);
  ntuple_->Branch("v_pv_lxy_DPlus", &v_pv_lxy_DPlus_);
  ntuple_->Branch("v_pv_lxy_D0", &v_pv_lxy_D0_);
  ntuple_->Branch("v_pv_lxy_Dstar_D0", &v_pv_lxy_Dstar_D0_);
  ntuple_->Branch("v_pv_lxy_DsPlus", &v_pv_lxy_DsPlus_);
  ntuple_->Branch("v_pv_lxy_LambdacPlus", &v_pv_lxy_LambdacPlus_);
}

TopAnalysis::~TopAnalysis()
{
}

// ------------ method called for each event  ------------
void
TopAnalysis::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  run_ = iEvent.id().run();
  event_ = iEvent.id().event();
  lumi_ = iEvent.id().luminosityBlock();

  //cout<<"TopAnalysis::analyze()"<<endl;
  //cout<<"Run "<<run_<<"  Event "<<event_<<"  Lumi "<<lumi_<<endl;

  edm::Handle<edm::View<reco::Vertex> > vertexHandle_;
  iEvent.getByToken(vertexToken_, vertexHandle_);
  //cout<<"Vertex collection size = "<<vertexHandle_->size()<<endl;
  nPVertex_ = vertexHandle_->size();
  if(nPVertex_ == 0) return;
  int iv = 0;
  double zpv = 0;
  _pv = NULL;
  View<reco::Vertex>::const_iterator iVertex;
  for(iVertex = vertexHandle_->begin(); iVertex != vertexHandle_->end(); iVertex++) {
    if(iv == 2) break;
    if(iv == 0) {
      mVertex1_ = iVertex->p4().mass();
      zpv = iVertex->z();
      _pv = &(*iVertex);
    }
    if(iv == 1) {
      mVertex2_ = iVertex->p4().mass();
    }
    iv += 1;
  }
  zPVertex_ = zpv;

  edm::Handle<edm::View<reco::Track>> trackHandle_;
  iEvent.getByToken(generalTracksToken_,trackHandle_);
  //cout<<"Track collection size = "<<trackHandle_->size()<<endl;
  for(View<reco::Track>::const_iterator iTrack = trackHandle_->begin();
        iTrack != trackHandle_->end(); iTrack++) {
    vxTrack_ = iTrack->vx();
    vyTrack_ = iTrack->vy();
    dz_trackPV_ = iTrack->vz()-zpv;
  }

  edm::Handle<edm::View<reco::PFMET> > metHandle_;
  iEvent.getByToken(metToken_,metHandle_);
  //cout<<"MET collection size = "<<metHandle_->size()<<endl;
  const reco::PFMET &met = metHandle_->front();
  pfmet_ = met.pt();

  edm::Handle<edm::View<reco::Muon> > muonsHandle_;
  iEvent.getByToken(muonsToken_,muonsHandle_);
  //cout<<"Muon collection size = "<<muonsHandle_->size()<<endl;
  std::vector<const reco::Muon *> muons;
  for(size_t i=0; i<muonsHandle_->size(); ++i) {
    const auto mu = muonsHandle_->ptrAt(i);
    muons.push_back(&(*mu));
  }
  std::sort(muons.begin(),muons.end(),comp_muon_pt());
  mumu = false;
  zmm = false;
  for(unsigned j=1; j<muons.size(); j++) {
    if(muons[j]->pt() < 20.0 ) continue;
    if(fabs(muons[j]->eta()) > 2.4) continue;
    if(!isMuon(muons[j])) continue;
    double isomu2 = (muons[j]->pfIsolationR04().sumChargedHadronPt + max(0.,
                     muons[j]->pfIsolationR04().sumNeutralHadronEt +
                     muons[j]->pfIsolationR04().sumPhotonEt - 0.5 *
                     muons[j]->pfIsolationR04().sumPUPt))/muons[j]->pt();
    double e2 = sqrt(muons[j]->px()*muons[j]->px()+
                     muons[j]->py()*muons[j]->py()+
                     muons[j]->pz()*muons[j]->pz()+mmu*mmu);
    for(unsigned i=0; i<j; i++) {
      if(muons[i]->pt() < 25.0) continue;
      if(fabs(muons[i]->eta()) > 2.4) continue;
      if(!isMuon(muons[i])) continue;
      double isomu1 = (muons[i]->pfIsolationR04().sumChargedHadronPt + max(0.,
                       muons[i]->pfIsolationR04().sumNeutralHadronEt +
                       muons[i]->pfIsolationR04().sumPhotonEt - 0.5 *
                       muons[i]->pfIsolationR04().sumPUPt))/muons[i]->pt();
      if(muons[i]->charge()*muons[j]->charge() > 0) continue;
      if(isomu1 > 0.15) continue;
      if(isomu2 > 0.15) continue;
      double e1 = sqrt(muons[i]->px()*muons[i]->px()+
                       muons[i]->py()*muons[i]->py()+
                       muons[i]->pz()*muons[i]->pz()+mmu*mmu);
      LV lvm1, lvm2, lvmm;
      lvm1.SetPxPyPzE(muons[i]->px(),muons[i]->py(),muons[i]->pz(),e1);
      lvm2.SetPxPyPzE(muons[j]->px(),muons[j]->py(),muons[j]->pz(),e2);
      lvmm = lvm1+lvm2;
      if(lvmm.M() < 20.) continue;    
      if(lvmm.M() > 76. && lvmm.M() < 106.) zmm = true;
      mumu = true;
    }
  }
  //if(zmm) cout<<"  found z boson"<<endl;
  //if(mumu) cout<<"  found di-muon"<<endl;

  edm::Handle<edm::View<reco::GsfElectron> > electronsHandle_;
  iEvent.getByToken(electronsToken_,electronsHandle_);
  edm::Handle<edm::ValueMap<bool> > electronId_;
  iEvent.getByToken(electronIdToken_,electronId_);
  //cout<<"Electron collection size = "<<electronsHandle_->size()<<endl;  
  std::vector<const reco::GsfElectron *> electrons;
  bool isEle = false;
  bool barrel, endcap = false;
  for(size_t i=0; i<electronsHandle_->size(); ++i) {
    const auto el = electronsHandle_->ptrAt(i);
    if(fabs(el->superCluster()->eta()) > 1.4442 && 
       fabs(el->superCluster()->eta()) < 1.5660) continue;
    isEle = (*electronId_)[el];
    if(!isEle) continue;
    if(fabs(el->superCluster()->eta()) < 1.4442 &&
       fabs(el->gsfTrack()->dxy(_pv->position())) < 0.05 &&
       fabs(el->gsfTrack()->dz(_pv->position())) < 0.10) barrel = true;
    if(fabs(el->superCluster()->eta()) > 1.5660 &&
       fabs(el->gsfTrack()->dxy(_pv->position())) < 0.10 && 
       fabs(el->gsfTrack()->dz(_pv->position())) < 0.20) endcap = true;
    if(!barrel) continue;
    if(!endcap) continue;
    electrons.push_back(&(*el));
  }
  std::sort(electrons.begin(),electrons.end(),comp_electron_pt());
  ee = false;
  zee = false;
  for(unsigned j=1; j<electrons.size(); j++) {
    if(electrons[j]->pt() < 20.0) continue;
    if(fabs(electrons[j]->eta()) > 2.4) continue;
    double e2 = sqrt(electrons[j]->px()*electrons[j]->px()+
                     electrons[j]->py()*electrons[j]->py()+
                     electrons[j]->pz()*electrons[j]->pz()+me*me);
    for(unsigned i=0; i<j; i++) {
      if(electrons[i]->pt() < 25.0) continue;
      if(fabs(electrons[i]->eta()) > 2.4) continue;
      if(electrons[i]->charge()*electrons[j]->charge() > 0) continue;
      double e1 = sqrt(electrons[i]->px()*electrons[i]->px()+
                       electrons[i]->py()*electrons[i]->py()+
                       electrons[i]->pz()*electrons[i]->pz()+me*me);
      LV lve1, lve2, lvee;
      lve1.SetPxPyPzE(electrons[i]->px(),electrons[i]->py(),electrons[i]->pz(),e1);
      lve2.SetPxPyPzE(electrons[j]->px(),electrons[j]->py(),electrons[j]->pz(),e2);
      lvee = lve1+lve2;
      if(lvee.M() < 20.) continue;
      if(lvee.M() > 76. && lvee.M() < 106.) zee = true;
      ee = true;
    }
  }
  //if(zee) cout<<"  found z boson"<<endl;
  //if(ee) cout<<"  found di-electron"<<endl;

  emu = false;
  for(unsigned j=0; j<muons.size(); j++) {
    if(muons[j]->pt() < 20.) continue;
    if(fabs(muons[j]->eta()) > 2.4) continue;
    if(!isMuon(muons[j])) continue;
    double isomu = (muons[j]->pfIsolationR04().sumChargedHadronPt + max(0.,
                    muons[j]->pfIsolationR04().sumNeutralHadronEt +
                    muons[j]->pfIsolationR04().sumPhotonEt - 0.5 *
                    muons[j]->pfIsolationR04().sumPUPt))/muons[j]->pt();
    double e2 = sqrt(muons[j]->px()*muons[j]->px()+
                     muons[j]->py()*muons[j]->py()+
                     muons[j]->pz()*muons[j]->pz()+mmu*mmu);
    for(unsigned i=0; i<electrons.size(); i++) {
      if(electrons[i]->pt() < 25.0) continue;
      if(fabs(electrons[i]->eta()) > 2.4) continue;
      if(muons[j]->charge()*electrons[i]->charge() > 0) continue;
      if(isomu > 0.15) continue;
      double e1 = sqrt(electrons[i]->px()*electrons[i]->px()+
                       electrons[i]->py()*electrons[i]->py()+
                       electrons[i]->pz()*electrons[i]->pz()+me*me);
      LV lve1, lvm2, lvem;
      lve1.SetPxPyPzE(electrons[i]->px(),electrons[i]->py(),electrons[i]->pz(),e1);
      lvm2.SetPxPyPzE(muons[j]->px(),muons[j]->py(),muons[j]->pz(),e2);
      lvem = lve1+lvm2;
      if(lvem.M() < 20.) continue;
      emu = true;
    }
  }
  //if(emu) cout<<"  found e-mu"<<endl;

  mue = false;
  for(unsigned j=0; j<electrons.size(); j++) {
    if(electrons[j]->pt() < 20.0) continue;
    if(fabs(electrons[j]->eta()) > 2.4) continue;
    double e2 = sqrt(electrons[j]->px()*electrons[j]->px()+
                     electrons[j]->py()*electrons[j]->py()+
                     electrons[j]->pz()*electrons[j]->pz()+me*me);
    for(unsigned i=0; i<muons.size(); i++) {
      if(muons[i]->pt() < 25.0) continue;
      if(fabs(muons[i]->eta()) > 2.4) continue;
      if(!isMuon(muons[i])) continue;
      double isomu = (muons[i]->pfIsolationR04().sumChargedHadronPt + max(0.,
                      muons[i]->pfIsolationR04().sumNeutralHadronEt +
                      muons[i]->pfIsolationR04().sumPhotonEt - 0.5 *
                      muons[i]->pfIsolationR04().sumPUPt))/muons[i]->pt();
      if(muons[i]->charge()*electrons[j]->charge() > 0) continue;
      if(isomu > 0.15) continue;
      double e1 = sqrt(muons[i]->px()*muons[i]->px()+
                       muons[i]->py()*muons[i]->py()+
                       muons[i]->pz()*muons[i]->pz()+mmu*mmu);
      LV lvm1, lve2, lvme;
      lvm1.SetPxPyPzE(muons[i]->px(),muons[i]->py(),muons[i]->pz(),e1);
      lve2.SetPxPyPzE(electrons[j]->px(),electrons[j]->py(),electrons[j]->pz(),e2);
      lvme = lvm1+lve2;
      if(lvme.M() < 20.) continue;
      mue = true;
    }
  }
  //if(mue) cout<<"  found mu-e"<<endl;

  edm::Handle<edm::View<reco::GenParticle>> genHandle_;
  iEvent.getByToken(genToken_,genHandle_);
  if(misMC) {
    GenAnalysis(iEvent,iSetup,genHandle_);
    RecoAnalysis(iEvent,iSetup,trackHandle_);

    // Analyze lepton finding efficiency
    int nlep = 0;
    for(unsigned int i=0; i<bottom_hadrons.size(); i++) {
      const reco::Candidate *hb = bottom_hadrons[i];
      if(hb != NULL) {
        //cout<<"B decay products:";
        for(unsigned int j=0; j<hb->numberOfDaughters(); j++) {
          //cout<<" "<<hb->daughter(j)->pdgId();
          if(hb->daughter(j)->pt() < 1.) continue;
          if(fabs(hb->daughter(j)->pt()) > 2.5) continue;
          if(abs(hb->daughter(j)->pdgId()) == 11 ||
             abs(hb->daughter(j)->pdgId()) == 13) {
            nlep += 1;
            pt_gen_lepton_.push_back(hb->daughter(j)->pt());
            eta_gen_lepton_.push_back(hb->daughter(j)->eta());
            dz_gen_leptonPV_.push_back(hb->daughter(j)->vz()-zpv);
            if(abs(hb->daughter(j)->pdgId()) == 13) p4_gen_muon_.push_back(this->getLV(hb->daughter(j)->p4()));
            if(abs(hb->daughter(j)->pdgId()) == 11) p4_gen_electron_.push_back(this->getLV(hb->daughter(j)->p4()));
            std::vector<const reco::Track *>match = matchingTrack(hb->daughter(j),trackHandle_);
            if(!match.empty()) {
              if(abs(hb->daughter(j)->pdgId()) == 13)
                p4_gen_matched_muon_.push_back(this->getLV(hb->daughter(j)->p4()));
              if(abs(hb->daughter(j)->pdgId()) == 11)
                p4_gen_matched_electron_.push_back(this->getLV(hb->daughter(j)->p4()));
            }
          }
        }
        //cout<<endl;
      }
      else {
        leptons.push_back(NULL);
        charm_hadrons.push_back(NULL);
      }
    }
    //cout<<"found "<<nlep<<" leptons from semi-leptonic B decays"<<endl;
  }
  else {
    DataAnalysis(iEvent,iSetup,muonsHandle_,electronsHandle_);
  }
  //cout<<""<<endl;
  ntuple_->Fill();

  // Clear the variables
  nkpi = 0;
  nkpipi = 0;
  nkkpi = 0;
  ndstar = 0;
  npkpi = 0;
  top_quarks.clear();
  bottom_hadrons.clear();
  charm_hadrons.clear();
  leptons.clear();
  dp_hadron.clear();
  lepton_dp.clear();
  kaon_dp.clear();
  pion1_dp.clear();
  pion2_dp.clear();
  d0_hadron.clear();
  lepton_d0.clear();
  kaon_d0.clear();
  pion_d0.clear();
  dt_hadron.clear();
  lepton_dt.clear();
  kaon_dt.clear();
  pion_dt.clear();
  pionsoft_dt.clear();
  ds_hadron.clear();
  lepton_ds.clear();
  kaon1_ds.clear();
  kaon2_ds.clear();
  pion_ds.clear();
  lambda_hadron.clear();
  lepton_lambda.clear();
  kaon_lambda.clear();
  pion_lambda.clear();
  proton_lambda.clear();
  par.clear();
  covm.clear();
  ldp_par.clear();
  kdp_par.clear();
  pi1dp_par.clear();
  pi2dp_par.clear();
  kd0_par.clear();
  pid0_par.clear();
  kdt_par.clear();
  pidt_par.clear();
  pisdt_par.clear();
  k1ds_par.clear();
  k2ds_par.clear();
  pids_par.clear();
  klambda_par.clear();
  pilambda_par.clear();
  plambda_par.clear();
  ldp_cov.clear();
  kdp_cov.clear();
  pi1dp_cov.clear();
  pi2dp_cov.clear();
  kd0_cov.clear();
  pid0_cov.clear();
  kdt_cov.clear();
  pidt_cov.clear();
  pisdt_cov.clear();
  k1ds_cov.clear();
  k2ds_cov.clear();
  pids_cov.clear();
  klambda_cov.clear();
  pilambda_cov.clear();
  plambda_cov.clear();

  // Branch variables
  run_ = 0;
  lumi_ = 0;
  event_ = 0;
  nPVertex_ = 0;
  mVertex1_ = 0;
  mVertex2_ = 0;
  zPVertex_ = 0;
  vxTrack_ = 0;
  vyTrack_ = 0;
  dz_trackPV_ = 0;
  pfmet_ = 0;
  nTrackJet1_ = 0;
  nTrackJet2_ = 0;
  nJetPV_ = 0;
  pt_gen_lepton_.clear();
  eta_gen_lepton_.clear();
  dz_gen_leptonPV_.clear();
  p4_gen_muon_.clear();
  p4_gen_electron_.clear();
  p4_gen_matched_muon_.clear();
  p4_gen_matched_electron_.clear();
  p4_DPlus_.clear();
  p4_DPlus_kaon_.clear();
  p4_DPlus_pion1_.clear();
  p4_DPlus_pion2_.clear();
  v_charge_DPlus_kaon_.clear();
  v_charge_DPlus_pion1_.clear();
  v_charge_DPlus_pion2_.clear();
  p4_D0_.clear();
  p4_D0_kaon_.clear();
  p4_D0_pion_.clear();
  v_charge_D0_kaon_.clear();
  v_charge_D0_pion_.clear();
  mdiff_Dstar_D0_.clear();
  p4_Dstar_.clear();
  p4_Dstar_D0_.clear();
  p4_Dstar_D0_kaon_.clear();
  p4_Dstar_D0_pion_.clear();
  p4_Dstar_pionsoft_.clear();
  v_charge_Dstar_D0_kaon_.clear();
  v_charge_Dstar_D0_pion_.clear();
  v_charge_Dstar_pionsoft_.clear();
  p4_DsPlus_.clear();
  p4_DsPlus_kaon1_.clear();
  p4_DsPlus_kaon2_.clear();
  p4_DsPlus_pion_.clear();
  v_charge_DsPlus_kaon1_.clear();
  v_charge_DsPlus_kaon2_.clear();
  v_charge_DsPlus_pion_.clear();
  p4_LambdacPlus_.clear();
  p4_LambdacPlus_kaon_.clear();
  p4_LambdacPlus_pion_.clear();
  p4_LambdacPlus_proton_.clear();
  v_charge_LambdacPlus_kaon_.clear();
  v_charge_LambdacPlus_pion_.clear();
  v_charge_LambdacPlus_proton_.clear();
  v_px_DPlus_lepton_.clear();
  v_py_DPlus_lepton_.clear();
  v_pz_DPlus_lepton_.clear();
  v_pt_DPlus_lepton_.clear();
  v_eta_DPlus_lepton_.clear();
  v_phi_DPlus_lepton_.clear();
  v_mass_DPlus_lepton_.clear();
  v_energy_DPlus_lepton_.clear();
  v_charge_DPlus_lepton_.clear();
  v_px_D0_lepton_.clear();
  v_py_D0_lepton_.clear();
  v_pz_D0_lepton_.clear();
  v_pt_D0_lepton_.clear();
  v_eta_D0_lepton_.clear();
  v_phi_D0_lepton_.clear();
  v_mass_D0_lepton_.clear();
  v_energy_D0_lepton_.clear();
  v_charge_D0_lepton_.clear();
  v_px_Dstar_lepton_.clear();
  v_py_Dstar_lepton_.clear();
  v_pz_Dstar_lepton_.clear();
  v_pt_Dstar_lepton_.clear();
  v_eta_Dstar_lepton_.clear();
  v_phi_Dstar_lepton_.clear();
  v_mass_Dstar_lepton_.clear();
  v_energy_Dstar_lepton_.clear();
  v_charge_Dstar_lepton_.clear();
  v_px_DsPlus_lepton_.clear();
  v_py_DsPlus_lepton_.clear();
  v_pz_DsPlus_lepton_.clear();
  v_pt_DsPlus_lepton_.clear();
  v_eta_DsPlus_lepton_.clear();
  v_phi_DsPlus_lepton_.clear();
  v_mass_DsPlus_lepton_.clear();
  v_energy_DsPlus_lepton_.clear();
  v_charge_DsPlus_lepton_.clear();
  v_px_LambdacPlus_lepton_.clear();
  v_py_LambdacPlus_lepton_.clear();
  v_pz_LambdacPlus_lepton_.clear();
  v_pt_LambdacPlus_lepton_.clear();
  v_eta_LambdacPlus_lepton_.clear();
  v_phi_LambdacPlus_lepton_.clear();
  v_mass_LambdacPlus_lepton_.clear();
  v_energy_LambdacPlus_lepton_.clear();
  v_charge_LambdacPlus_lepton_.clear();
  v_trkpara_DPlus_kaon_.clear();
  v_trkpara_DPlus_pion1_.clear();
  v_trkpara_DPlus_pion2_.clear();
  v_trkcovm_DPlus_kaon_.clear();
  v_trkcovm_DPlus_pion1_.clear();
  v_trkcovm_DPlus_pion2_.clear();
  v_trkpara_D0_kaon_.clear();
  v_trkpara_D0_pion_.clear();
  v_trkcovm_D0_kaon_.clear();
  v_trkcovm_D0_pion_.clear();
  v_trkpara_Dstar_D0_kaon_.clear();
  v_trkpara_Dstar_D0_pion_.clear();
  v_trkpara_Dstar_pionsoft_.clear();
  v_trkcovm_Dstar_D0_kaon_.clear();
  v_trkcovm_Dstar_D0_pion_.clear();
  v_trkcovm_Dstar_pionsoft_.clear();
  v_trkpara_DsPlus_kaon1_.clear();
  v_trkpara_DsPlus_kaon2_.clear();
  v_trkpara_DsPlus_pion_.clear();
  v_trkcovm_DsPlus_kaon1_.clear();
  v_trkcovm_DsPlus_kaon2_.clear();
  v_trkcovm_DsPlus_pion_.clear();
  v_trkpara_LambdacPlus_kaon_.clear();
  v_trkpara_LambdacPlus_pion_.clear();
  v_trkpara_LambdacPlus_proton_.clear();
  v_trkcovm_LambdacPlus_kaon_.clear();
  v_trkcovm_LambdacPlus_pion_.clear();
  v_trkcovm_LambdacPlus_proton_.clear();
  v_costheta_LambdacPlus_lepton_.clear();
  v_pv_lxy_DPlus_.clear();
  v_pv_lxy_D0_.clear();
  v_pv_lxy_Dstar_D0_.clear();
  v_pv_lxy_DsPlus_.clear();
  v_pv_lxy_LambdacPlus_.clear();
}

void TopAnalysis::GenAnalysis(const edm::Event& iEvent,
                              const edm::EventSetup& iSetup,
                              edm::Handle<edm::View<reco::GenParticle> > &handle) {
  for(size_t igen=0; igen<handle->size(); igen++) {
    const Candidate *bhadron = &(*handle)[igen];
    if(abs(bhadron->pdgId()) == 511 ||
       abs(bhadron->pdgId()) == 521 ||
       abs(bhadron->pdgId()) == 531 ||
       abs(bhadron->pdgId()) == 5122) {
      if(!isTopAncestor(bhadron)) continue;
      bottom_hadrons.push_back(bhadron);
      for(size_t ilep=0; ilep<bhadron->numberOfDaughters(); ilep++) {
        const Candidate *lepton = bhadron->daughter(ilep);
        if(abs(lepton->pdgId()) == 11 ||
           abs(lepton->pdgId()) == 13) {
           leptons.push_back(lepton);
          for(size_t ichad=0; ichad<bhadron->numberOfDaughters(); ichad++) {
            if(ilep == ichad) continue;
            const Candidate *chadron = bhadron->daughter(ichad);
            if(abs(chadron->pdgId()) == 411 ||
               abs(chadron->pdgId()) == 413 ||
               abs(chadron->pdgId()) == 421 ||
               abs(chadron->pdgId()) == 431 ||
               abs(chadron->pdgId()) == 4122) {
              charm_hadrons.push_back(chadron);
              if(abs(chadron->pdgId()) == 411) {
                for(size_t idau=0; idau<chadron->numberOfDaughters(); idau++) {
                  const Candidate *k = chadron->daughter(idau);
                  if(lepton->pdgId()*k->pdgId() == -11*321 ||
                     lepton->pdgId()*k->pdgId() == -13*321) {
                    for(size_t jdau=0; jdau<chadron->numberOfDaughters(); jdau++) {
                      if(idau == jdau) continue;
                      const Candidate *pi1 = chadron->daughter(jdau);
                      if(k->pdgId()*pi1->pdgId() != -321*211) continue;
                      for(size_t kdau=0; kdau<jdau; kdau++) {
                        if(jdau == kdau) continue;
                        const Candidate *pi2 = chadron->daughter(kdau);
                        if(k->pdgId()*pi2->pdgId() != -321*211) continue;
                        dp_hadron.push_back(chadron);
                        lepton_dp.push_back(lepton);
                        kaon_dp.push_back(k);
                        pion1_dp.push_back(pi1);
                        pion2_dp.push_back(pi2);
                      } // second pion candidate
                    } // first pion candidate
                  } // lepton and kaon have same charge
                } // kaon candidate
              } // d+ chandidate

              if(abs(chadron->pdgId()) == 421) {
                for(size_t idau=0; idau<chadron->numberOfDaughters(); idau++) {
                  const Candidate *k = chadron->daughter(idau);
                  if(lepton->pdgId()*k->pdgId() == -11*321 ||
                     lepton->pdgId()*k->pdgId() == -13*321) {
                    for(size_t jdau=0; jdau<chadron->numberOfDaughters(); jdau++) {
                      if(idau == jdau) continue;
                      const Candidate *pi = chadron->daughter(jdau);
                      if(k->pdgId()*pi->pdgId() != -321*211) continue;
                      d0_hadron.push_back(chadron);
                      lepton_d0.push_back(lepton);
                      kaon_d0.push_back(k);
                      pion_d0.push_back(pi);
                    } // pion candidate
                  } // lepton and kaon have same charge
                } // kaon candidate
              } // d0 candidate

              if(abs(chadron->pdgId()) == 413) {
                for(size_t idau=0; idau<chadron->numberOfDaughters(); idau++) {
                  const Candidate *pis = chadron->daughter(idau);
                  if(abs(pis->pdgId()) == 211) {
                    for(size_t cdau = 0; cdau<chadron->numberOfDaughters(); cdau++) {
                      if(idau == cdau) continue;
                      if(abs(chadron->daughter(cdau)->pdgId()) == 421) {
                        for(size_t jdau=0; jdau<chadron->daughter(cdau)->numberOfDaughters(); jdau++) {
                          const Candidate *k = chadron->daughter(cdau)->daughter(jdau);
                          if(lepton->pdgId()*k->pdgId() == -11*321 ||
                             lepton->pdgId()*k->pdgId() == -13*321) {
                            for(size_t kdau=0; kdau<chadron->daughter(cdau)->numberOfDaughters(); kdau++) {
                              if(jdau == kdau) continue;
                              const Candidate *pi = chadron->daughter(cdau)->daughter(kdau);
                              if(k->pdgId()*pi->pdgId() != -321*211) continue;
                              dt_hadron.push_back(chadron);
                              lepton_dt.push_back(lepton);
                              kaon_dt.push_back(k);
                              pion_dt.push_back(pi);
                              pionsoft_dt.push_back(pis);
                            } // pion candidate
                          } // lepton and kaon have same charge
                        } // kaon candidate
                      } // d0 candidate
                    } // d*+ daughters
                  } // soft pion
                } // d*+ daughters
              } // d*+ candidate

              if(abs(chadron->pdgId()) == 431) {
                for(size_t idau=0; idau<chadron->numberOfDaughters(); idau++) {
                  const Candidate *pi = chadron->daughter(idau);
                  if(lepton->pdgId()*pi->pdgId() == 11*211 ||
                     lepton->pdgId()*pi->pdgId() == 13*211) {
                    for(size_t jdau=0; jdau<chadron->numberOfDaughters(); jdau++) {
                      if(idau == jdau) continue;
                      const Candidate *k1 = chadron->daughter(jdau);
                      for(size_t kdau=0; kdau<jdau; kdau++) {
                        if(jdau == kdau) continue;
                        const Candidate *k2 = chadron->daughter(kdau);
                        if(k1->pdgId()*k2->pdgId() != -321*321) continue;
                        ds_hadron.push_back(chadron);
                        lepton_ds.push_back(lepton);
                        kaon1_ds.push_back(k1);
                        kaon2_ds.push_back(k2);
                        pion_ds.push_back(pi);
                      } // second kaon candidate
                    } // first kaon candidate
                  } // lepton and pion have opposite charge
                } // pion candidate 
              } // ds+ candidate

              if(abs(chadron->pdgId()) == 4122) {
                for(size_t idau=0; idau<chadron->numberOfDaughters(); idau++) {
                  const Candidate *proton = chadron->daughter(idau);
                  if(lepton->pdgId()*proton->pdgId() == 11*2212 ||
                     lepton->pdgId()*proton->pdgId() == 13*2212) {
                    for(size_t ksdau=0; ksdau<chadron->numberOfDaughters(); ksdau++) {
                      if(idau == ksdau) continue;
                      if(abs(chadron->daughter(ksdau)->pdgId()) == 313) {
                        for(size_t jdau=0; jdau<chadron->daughter(ksdau)->numberOfDaughters(); jdau++) {
                          const Candidate *k = chadron->daughter(ksdau)->daughter(jdau);
                          if(lepton->pdgId()*k->pdgId() == -11*321 ||
                             lepton->pdgId()*k->pdgId() == -13*321) {
                            for(size_t kdau=0; kdau<chadron->daughter(ksdau)->numberOfDaughters(); kdau++) {
                              if(jdau == kdau) continue;
                              const Candidate *pi = chadron->daughter(ksdau)->daughter(kdau);
                              if(k->pdgId()*pi->pdgId() != -321*211) continue;
                              lambda_hadron.push_back(chadron);
                              lepton_lambda.push_back(lepton);
                              kaon_lambda.push_back(k);
                              pion_lambda.push_back(pi);
                              proton_lambda.push_back(proton);
                            } // pion candidate
                          } // kaon and lepton have same charge
                        } // kaon candidate
                      } // k*0 candidate
                    } // lambda_c+ daughters
                  } // lepton and proton have opposite charge
                } // proton candidate
              } // lambda_c+ candidate
            } // charm hadrons
          } // b daughters
        } // leptons
      } // b daughters
    } // b0 or b+ or bs+ or lambda_b+ hadron
  } // gen particles
  //cout<<"Number of top quarks: "<<top_quarks.size()<<endl;
  //cout<<"Number of bottom hadrons: "<<bottom_hadrons.size()<<endl;
}

void TopAnalysis::RecoAnalysis(const edm::Event& iEvent,
                               const edm::EventSetup& iSetup,
                               edm::Handle<edm::View<reco::Track> > &handle) {
  //if(!handle.isValid()) cout<<"No tracks information in AOD: "<<run_<<","<<event_<<endl;
  if(!handle.isValid()) return;
  double ml = 0;
  for(unsigned int igen=0; igen<dp_hadron.size(); igen++) {
    nkpipi += 1;
    //cout<<"  found D+ hadron: "<<dp_hadron.at(igen)->pdgId()<<endl;
    std::vector<const reco::Track*> tl = matchingTrack(lepton_dp.at(igen),handle);
    std::vector<const reco::Track*> tk = matchingTrack(kaon_dp.at(igen),handle);
    std::vector<const reco::Track*> tpi1 = matchingTrack(pion1_dp.at(igen),handle);
    std::vector<const reco::Track*> tpi2 = matchingTrack(pion2_dp.at(igen),handle);
    for(unsigned int i=0; i<tk.size(); i++) {
      for(unsigned int j=0; j<tpi1.size(); j++) {
        for(unsigned int k=0; k<tpi2.size(); k++) {
          nlepton = 0;
          if(tk[i] == tpi1[j]) continue;
          if(tk[i] == tpi2[k]) continue;
          if(tpi1[j] == tpi2[k]) continue;
          if(tk[i]->charge()*tpi1[j]->charge() > 0) continue;
          if(tk[i]->charge()*tpi2[k]->charge() > 0) continue;
          double ek = sqrt(tk[i]->px()*tk[i]->px()+
                           tk[i]->py()*tk[i]->py()+
                           tk[i]->pz()*tk[i]->pz()+mk*mk);
          double epi1 = sqrt(tpi1[j]->px()*tpi1[j]->px()+
                             tpi1[j]->py()*tpi1[j]->py()+
                             tpi1[j]->pz()*tpi1[j]->pz()+mpi*mpi);
          double epi2 = sqrt(tpi2[k]->px()*tpi2[k]->px()+
                             tpi2[k]->py()*tpi2[k]->py()+
                             tpi2[k]->pz()*tpi2[k]->pz()+mpi*mpi);
          LV lvk, lvpi1, lvpi2, dp;
          lvk.SetPxPyPzE(tk[i]->px(),tk[i]->py(),tk[i]->pz(),ek);
          lvpi1.SetPxPyPzE(tpi1[j]->px(),tpi1[j]->py(),tpi1[j]->pz(),epi1);
          lvpi2.SetPxPyPzE(tpi2[k]->px(),tpi2[k]->py(),tpi2[k]->pz(),epi2);
          dp = lvk+lvpi1+lvpi2;
          if(dp.Pt() < 5.) continue;
          if(dp.M() < 1.7 || dp.M() > 2.0) continue;
          // Adding lepton from semi-leptonic b decay
          std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
          for(unsigned int l=0; l<tl.size(); l++) {
            if(tk[i] == tl[l]) continue;
            if(tpi1[j] == tl[l]) continue;
            if(tpi2[k] == tl[l]) continue;
            if(tk[i]->charge()*tl[l]->charge() < 0) continue;
            if(abs(lepton_dp.at(igen)->pdgId()) == 11) ml = me;
            else if(abs(lepton_dp.at(igen)->pdgId()) == 13) ml = mmu;
            double el = sqrt(tl[l]->px()*tl[l]->px()+
                             tl[l]->py()*tl[l]->py()+
                             tl[l]->pz()*tl[l]->pz()+ml*ml);
            LV lvl, b0;
            lvl.SetPxPyPzE(tl[l]->px(),tl[l]->py(),tl[l]->pz(),el);
            b0 = lvl+dp;
            if(b0.M() < 2.0 || b0.M() > 6.0) continue;
            nlepton += 1;
            px_lep.push_back(lvl.Px());
            py_lep.push_back(lvl.Py());
            pz_lep.push_back(lvl.Pz());
            pt_lep.push_back(lvl.Pt());
            eta_lep.push_back(lvl.Eta());
            phi_lep.push_back(lvl.Phi());
            mass_lep.push_back(lvl.M());
            energy_lep.push_back(lvl.E());
            charge_lep.push_back(tl[l]->charge());
          }
          if(nlepton < 1) continue;
          v_px_DPlus_lepton_.push_back(px_lep);
          v_py_DPlus_lepton_.push_back(py_lep);
          v_pz_DPlus_lepton_.push_back(pz_lep);
          v_pt_DPlus_lepton_.push_back(pt_lep);
          v_eta_DPlus_lepton_.push_back(eta_lep);
          v_phi_DPlus_lepton_.push_back(phi_lep);
          v_mass_DPlus_lepton_.push_back(mass_lep);
          v_energy_DPlus_lepton_.push_back(energy_lep);
          v_charge_DPlus_lepton_.push_back(charge_lep);
          p4_DPlus_.push_back(dp);
          p4_DPlus_kaon_.push_back(lvk);
          p4_DPlus_pion1_.push_back(lvpi1);
          p4_DPlus_pion2_.push_back(lvpi2);
          v_charge_DPlus_kaon_.push_back(tk[i]->charge());
          v_charge_DPlus_pion1_.push_back(tpi1[j]->charge());
          v_charge_DPlus_pion2_.push_back(tpi2[k]->charge());
          para.clear(); covmat.clear();
          v_trkpara_DPlus_kaon_ = unpackTrackParameters(kaon_dp.at(igen),nulltrack,"kaon_dp");
          v_trkpara_DPlus_pion1_ = unpackTrackParameters(pion1_dp.at(igen),nulltrack,"pion1_dp");
          v_trkpara_DPlus_pion2_ = unpackTrackParameters(pion2_dp.at(igen),nulltrack,"pion2_dp");
          v_trkcovm_DPlus_kaon_ = unpackTrackCovariance(tk[i],nulltrack,"kaon_dp");
          v_trkcovm_DPlus_pion1_ = unpackTrackCovariance(tpi1[j],nulltrack,"pion1_dp");
          v_trkcovm_DPlus_pion2_ = unpackTrackCovariance(tpi2[k],nulltrack,"pion2_dp");
          double pv_lxy = (_pv->x()*dp.Px()+
                           _pv->y()*dp.Py())/dp.Pt();
          v_pv_lxy_DPlus_.push_back(pv_lxy);
        }
      }
    }
  }
  for(unsigned int igen=0; igen<d0_hadron.size(); igen++) {
    nkpi += 1;
    //cout<<"found D0 hadron in track collection: "<<d0_hadron.at(igen)->pdgId()<<endl;
    std::vector<const reco::Track*> tl = matchingTrack(lepton_d0.at(igen),handle);
    std::vector<const reco::Track*> tk = matchingTrack(kaon_d0.at(igen),handle);
    std::vector<const reco::Track*> tpi = matchingTrack(pion_d0.at(igen),handle);
    for(unsigned int i=0; i<tk.size(); i++) {
      for(unsigned int j=0; j<tpi.size(); j++) {
        nlepton = 0;
        if(tk[i] == tpi[j]) continue;
        if(tk[i]->charge()*tpi[j]->charge() > 0) continue;
        double ek = sqrt(tk[i]->px()*tk[i]->px()+
                         tk[i]->py()*tk[i]->py()+
                         tk[i]->pz()*tk[i]->pz()+mk*mk);
        double epi = sqrt(tpi[j]->px()*tpi[j]->px()+
                          tpi[j]->py()*tpi[j]->py()+
                          tpi[j]->pz()*tpi[j]->pz()+mpi*mpi);
        LV lvk, lvpi, d0;
        lvk.SetPxPyPzE(tk[i]->px(),tk[i]->py(),tk[i]->pz(),ek);
        lvpi.SetPxPyPzE(tpi[j]->px(),tpi[j]->py(),tpi[j]->pz(),epi);
        d0 = lvk+lvpi;
        if(d0.Pt() < 5.) continue;
        if(d0.M() < 1.7 || d0.M() > 2.0) continue;
        // Adding lepton from semi-leptonic b decay
        std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
        for(unsigned int l=0; l<tl.size(); l++) {
          if(tk[i] == tl[l]) continue;
          if(tpi[j] == tl[l]) continue;
          if(tk[i]->charge()*tl[l]->charge() < 0) continue;
          if(abs(lepton_d0.at(igen)->pdgId()) == 11) ml = me;
          else if(abs(lepton_d0.at(igen)->pdgId()) == 13) ml = mmu;
          double el = sqrt(tl[l]->px()*tl[l]->px()+
                           tl[l]->py()*tl[l]->py()+
                           tl[l]->pz()*tl[l]->pz()+ml*ml);
          LV lvl, bp;
          lvl.SetPxPyPzE(tl[l]->px(),tl[l]->py(),tl[l]->pz(),el);
          bp = lvl+d0;
          if(bp.M() < 2.0 || bp.M() > 6.0) continue;
          nlepton += 1;
          px_lep.push_back(lvl.Px());
          py_lep.push_back(lvl.Py());
          pz_lep.push_back(lvl.Pz());
          pt_lep.push_back(lvl.Pt());
          eta_lep.push_back(lvl.Eta());
          phi_lep.push_back(lvl.Phi());
          mass_lep.push_back(lvl.M());
          energy_lep.push_back(lvl.E());
          charge_lep.push_back(tl[l]->charge());
        }
        if(nlepton < 1) continue;
        v_px_D0_lepton_.push_back(px_lep);
        v_py_D0_lepton_.push_back(py_lep);
        v_pz_D0_lepton_.push_back(pz_lep);
        v_pt_D0_lepton_.push_back(pt_lep);
        v_eta_D0_lepton_.push_back(eta_lep);
        v_phi_D0_lepton_.push_back(phi_lep);
        v_mass_D0_lepton_.push_back(mass_lep);
        v_energy_D0_lepton_.push_back(energy_lep);
        v_charge_D0_lepton_.push_back(charge_lep);
        p4_D0_.push_back(d0);
        p4_D0_kaon_.push_back(lvk);
        p4_D0_pion_.push_back(lvpi);
        v_charge_D0_kaon_.push_back(tk[i]->charge());
        v_charge_D0_pion_.push_back(tpi[j]->charge());
        para.clear(); covmat.clear();
        v_trkpara_D0_kaon_ = unpackTrackParameters(kaon_d0.at(igen),nulltrack,"kaon_d0");
        v_trkpara_D0_pion_ = unpackTrackParameters(pion_d0.at(igen),nulltrack,"pion_d0");
        v_trkcovm_D0_kaon_ = unpackTrackCovariance(tk[i],nulltrack,"kaon_d0");
        v_trkcovm_D0_pion_ = unpackTrackCovariance(tpi[j],nulltrack,"pion_d0");
        double pv_lxy = (_pv->x()*d0.Px()+
                         _pv->y()*d0.Py())/d0.Pt();
        v_pv_lxy_D0_.push_back(pv_lxy);
      }
    }
  }
  for(unsigned int igen=0; igen<dt_hadron.size(); igen++) {
    ndstar += 1;
    //cout<<"found D* hadron: "<<dt_hadron.at(igen)->pdgId()<<endl;
    std::vector<const reco::Track*> tl = matchingTrack(lepton_dt.at(igen),handle);
    std::vector<const reco::Track*> tk = matchingTrack(kaon_dt.at(igen),handle);
    std::vector<const reco::Track*> tpi = matchingTrack(pion_dt.at(igen),handle);
    std::vector<const reco::Track*> tpis = matchingTrack(pionsoft_dt.at(igen),handle);
    for(unsigned int i=0; i<tk.size(); i++) {
      for(unsigned int j=0; j<tpi.size(); j++) {
        for(unsigned int k=0; k<tpis.size(); k++) {
          nlepton = 0;
          if(tk[i] == tpi[j]) continue;
          if(tk[i] == tpis[k]) continue;
          if(tpi[j] == tpis[k]) continue;
          if(tk[i]->charge()*tpi[j]->charge() > 0) continue;
          if(tk[i]->charge()*tpis[k]->charge() > 0) continue;
          double ek = sqrt(tk[i]->px()*tk[i]->px()+
                           tk[i]->py()*tk[i]->py()+
                           tk[i]->pz()*tk[i]->pz()+mk*mk);
          double epi = sqrt(tpi[j]->px()*tpi[j]->px()+
                            tpi[j]->py()*tpi[j]->py()+
                            tpi[j]->pz()*tpi[j]->pz()+mpi*mpi);
          double epis = sqrt(tpis[k]->px()*tpis[k]->px()+
                            tpis[k]->py()*tpis[k]->py()+
                            tpis[k]->pz()*tpis[k]->pz()+mpi*mpi);
          LV lvk, lvpi, lvpis, d0, dt;
          lvk.SetPxPyPzE(tk[i]->px(),tk[i]->py(),tk[i]->pz(),ek);
          lvpi.SetPxPyPzE(tpi[j]->px(),tpi[j]->py(),tpi[j]->pz(),epi);
          lvpis.SetPxPyPzE(tpis[k]->px(),tpis[k]->py(),tpis[k]->pz(),epis);
          d0 = lvk+lvpi;
          dt = lvk+lvpi+lvpis;
          if(d0.Pt() < 5.) continue;
          if(d0.M() < 1.7 || d0.M() > 2.0) continue;
          // Adding lepton from semi-leptonic b decay
          std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
          for(unsigned int l=0; l<tl.size(); l++) {
            if(tk[i] == tl[l]) continue;
            if(tpi[j] == tl[l]) continue;
            if(tpis[k] == tl[l]) continue;
            if(tk[i]->charge()*tl[l]->charge() < 0) continue;
            if(abs(lepton_dt.at(igen)->pdgId()) == 11) ml = me;
            else if(abs(lepton_dt.at(igen)->pdgId()) == 13) ml = mmu;
            double el = sqrt(tl[l]->px()*tl[l]->px()+
                             tl[l]->py()*tl[l]->py()+
                             tl[l]->pz()*tl[l]->pz()+ml*ml);
            LV lvl, b0;
            lvl.SetPxPyPzE(tl[l]->px(),tl[l]->py(),tl[l]->pz(),el);
            b0 = lvl+dt;
            if(b0.M() < 2.0 || b0.M() > 6.0) continue;
            nlepton += 1;
            px_lep.push_back(lvl.Px());
            py_lep.push_back(lvl.Py());
            pz_lep.push_back(lvl.Pz());
            pt_lep.push_back(lvl.Pt());
            eta_lep.push_back(lvl.Eta());
            phi_lep.push_back(lvl.Phi());
            mass_lep.push_back(lvl.M());
            energy_lep.push_back(lvl.E());
            charge_lep.push_back(tl[l]->charge());
          }
          if(nlepton < 1) continue;
          v_px_Dstar_lepton_.push_back(px_lep);
          v_py_Dstar_lepton_.push_back(py_lep);
          v_pz_Dstar_lepton_.push_back(pz_lep);
          v_pt_Dstar_lepton_.push_back(pt_lep);
          v_eta_Dstar_lepton_.push_back(eta_lep);
          v_phi_Dstar_lepton_.push_back(phi_lep);
          v_mass_Dstar_lepton_.push_back(mass_lep);
          v_energy_Dstar_lepton_.push_back(energy_lep);
          v_charge_Dstar_lepton_.push_back(charge_lep);
          mdiff_Dstar_D0_.push_back(dt.M()-d0.M());
          p4_Dstar_.push_back(dt);
          p4_Dstar_D0_.push_back(d0);
          p4_Dstar_D0_kaon_.push_back(lvk);
          p4_Dstar_D0_pion_.push_back(lvpi);
          p4_Dstar_pionsoft_.push_back(lvpis);
          v_charge_Dstar_D0_kaon_.push_back(tk[i]->charge());
          v_charge_Dstar_D0_pion_.push_back(tpi[j]->charge());
          v_charge_Dstar_pionsoft_.push_back(tpis[k]->charge());
          para.clear(); covmat.clear();
          v_trkpara_Dstar_D0_kaon_ = unpackTrackParameters(kaon_dt.at(igen),nulltrack,"kaon_dt");
          v_trkpara_Dstar_D0_pion_ = unpackTrackParameters(pion_dt.at(igen),nulltrack,"pion_dt");
          v_trkpara_Dstar_pionsoft_ = unpackTrackParameters(pionsoft_dt.at(igen),nulltrack,"pionsoft_dt");
          v_trkcovm_Dstar_D0_kaon_ = unpackTrackCovariance(tk[i],nulltrack,"kaon_dt");
          v_trkcovm_Dstar_D0_pion_ = unpackTrackCovariance(tpi[j],nulltrack,"pion_dt");
          v_trkcovm_Dstar_pionsoft_ = unpackTrackCovariance(tpis[k],nulltrack,"pionsoft_dt");
          double pv_lxy = (_pv->x()*d0.Px()+
                           _pv->y()*d0.Py())/d0.Pt();
          v_pv_lxy_Dstar_D0_.push_back(pv_lxy);
        }
      }
    }
  }
  for(unsigned int igen=0; igen<ds_hadron.size(); igen++) {
    nkkpi += 1;
    //cout<<"found Ds hadron: "<<ds_hadron.at(igen)->pdgId()<<endl;
    std::vector<const reco::Track*> tl = matchingTrack(lepton_ds.at(igen),handle);
    std::vector<const reco::Track*> tk1 = matchingTrack(kaon1_ds.at(igen),handle);
    std::vector<const reco::Track*> tk2 = matchingTrack(kaon2_ds.at(igen),handle);
    std::vector<const reco::Track*> tpi = matchingTrack(pion_ds.at(igen),handle);
    for(unsigned int i=0; i<tk1.size(); i++) {
      for(unsigned int j=0; j<tk2.size(); j++) {
        for(unsigned int k=0; k<tpi.size(); k++) {
          nlepton = 0;
          if(tk1[i] == tk2[j]) continue;
          if(tk1[i] == tpi[k]) continue;
          if(tk2[j] == tpi[k]) continue;
          if(tk1[i]->charge()*tk2[j]->charge() > 0) continue;
          double ek1 = sqrt(tk1[i]->px()*tk1[i]->px()+tk1[i]->py()*tk1[i]->py()+tk1[i]->pz()*tk1[i]->pz()+mk*mk);
          double ek2 = sqrt(tk2[j]->px()*tk2[j]->px()+tk2[j]->py()*tk2[j]->py()+tk2[j]->pz()*tk2[j]->pz()+mk*mk);
          double epi = sqrt(tpi[k]->px()*tpi[k]->px()+tpi[k]->py()*tpi[k]->py()+tpi[k]->pz()*tpi[k]->pz()+mpi*mpi);
          LV lvk1, lvk2, lvpi, ds;
          lvk1.SetPxPyPzE(tk1[i]->px(),tk1[i]->py(),tk1[i]->pz(),ek1);
          lvk2.SetPxPyPzE(tk2[j]->px(),tk2[j]->py(),tk2[j]->pz(),ek2);
          lvpi.SetPxPyPzE(tpi[k]->px(),tpi[k]->py(),tpi[k]->pz(),epi);
          ds = lvk1+lvk2+lvpi;
          if(ds.Pt() < 5.) continue;
          if(ds.M() < 1.8 || ds.M() > 2.1) continue;
          // Adding lepton from semi-leptonic b decay
          std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
          for(unsigned int l=0; l<tl.size(); l++) {
            if(tk1[i] == tl[l]) continue;
            if(tk2[j] == tl[l]) continue;
            if(tpi[k] == tl[l]) continue;
            if(tpi[k]->charge()*tl[l]->charge() > 0) continue;
            if(abs(lepton_ds.at(igen)->pdgId()) == 11) ml = me;
            else if(abs(lepton_ds.at(igen)->pdgId()) == 13) ml = mmu;
            double el = sqrt(tl[l]->px()*tl[l]->px()+
                             tl[l]->py()*tl[l]->py()+
                             tl[l]->pz()*tl[l]->pz()+ml*ml);
            LV lvl, bs;
            lvl.SetPxPyPzE(tl[l]->px(),tl[l]->py(),tl[l]->pz(),el);
            bs = lvl+ds;
            if(bs.M() < 2.1 || bs.M() > 6.0) continue;
            nlepton += 1;
            px_lep.push_back(lvl.Px());
            py_lep.push_back(lvl.Py());
            pz_lep.push_back(lvl.Pz());
            pt_lep.push_back(lvl.Pt());
            eta_lep.push_back(lvl.Eta());
            phi_lep.push_back(lvl.Phi());
            mass_lep.push_back(lvl.M());
            energy_lep.push_back(lvl.E());
            charge_lep.push_back(tl[l]->charge());
          }
          if(nlepton < 1) continue;
          v_px_DsPlus_lepton_.push_back(px_lep);
          v_py_DsPlus_lepton_.push_back(py_lep);
          v_pz_DsPlus_lepton_.push_back(pz_lep);
          v_pt_DsPlus_lepton_.push_back(pt_lep);
          v_eta_DsPlus_lepton_.push_back(eta_lep);
          v_phi_DsPlus_lepton_.push_back(phi_lep);
          v_mass_DsPlus_lepton_.push_back(mass_lep);
          v_energy_DsPlus_lepton_.push_back(energy_lep);
          v_charge_DsPlus_lepton_.push_back(charge_lep);
          p4_DsPlus_.push_back(ds);
          p4_DsPlus_kaon1_.push_back(lvk1);
          p4_DsPlus_kaon2_.push_back(lvk2);
          p4_DsPlus_pion_.push_back(lvpi);
          v_charge_DsPlus_kaon1_.push_back(tk1[i]->charge());
          v_charge_DsPlus_kaon2_.push_back(tk2[j]->charge());
          v_charge_DsPlus_pion_.push_back(tpi[k]->charge());
          para.clear(); covmat.clear();
          v_trkpara_DsPlus_kaon1_ = unpackTrackParameters(kaon1_ds.at(igen),nulltrack,"kaon1_ds");
          v_trkpara_DsPlus_kaon2_ = unpackTrackParameters(kaon2_ds.at(igen),nulltrack,"kaon2_ds");
          v_trkpara_DsPlus_pion_ = unpackTrackParameters(pion_ds.at(igen),nulltrack,"pion_ds");
          v_trkcovm_DsPlus_kaon1_ = unpackTrackCovariance(tk1[i],nulltrack,"kaon1_ds");
          v_trkcovm_DsPlus_kaon2_ = unpackTrackCovariance(tk2[j],nulltrack,"kaon2_ds");
          v_trkcovm_DsPlus_pion_ = unpackTrackCovariance(tpi[k],nulltrack,"pion_ds");
          double pv_lxy = (_pv->x()*ds.Px()+
                           _pv->y()*ds.Py())/ds.Pt();
          v_pv_lxy_DsPlus_.push_back(pv_lxy);
        }
      }
    }
  }
  for(unsigned int igen=0; igen<lambda_hadron.size(); igen++) {
    npkpi += 1;
    //cout<<"found Lambda b0 hadron: "<<lambda_hadron.at(igen)->pdgId()<<endl;
    std::vector<const reco::Track*> tl = matchingTrack(lepton_lambda.at(igen),handle);
    std::vector<const reco::Track*> tk = matchingTrack(kaon_lambda.at(igen),handle);
    std::vector<const reco::Track*> tpi = matchingTrack(pion_lambda.at(igen),handle);
    std::vector<const reco::Track*> tp = matchingTrack(proton_lambda.at(igen),handle);
    for(unsigned int i=0; i<tk.size(); i++) {
      for(unsigned int j=0; j<tpi.size(); j++) {
        for(unsigned int k=0; k<tp.size(); k++) {
          nlepton = 0;
          if(tk[i] == tpi[j]) continue;
          if(tk[i] == tp[k]) continue;
          if(tpi[j] == tp[k]) continue;
          if(tk[i]->charge()*tpi[j]->charge() > 0) continue;
          if(tk[i]->charge()*tp[k]->charge() > 0) continue;
          double ek = sqrt(tk[i]->px()*tk[i]->px()+tk[i]->py()*tk[i]->py()+tk[i]->pz()*tk[i]->pz()+mk*mk);
          double epi = sqrt(tpi[j]->px()*tpi[j]->px()+tpi[j]->py()*tpi[j]->py()+
                            tpi[j]->pz()*tpi[j]->pz()+mpi*mpi);
          double ep = sqrt(tp[k]->px()*tp[k]->px()+tp[k]->py()*tp[k]->py()+
                           tp[k]->pz()*tp[k]->pz()+mp*mp);
          LV lvk, lvpi, lvp, lambdac;
          lvk.SetPxPyPzE(tk[i]->px(),tk[i]->py(),tk[i]->pz(),ek);
          lvpi.SetPxPyPzE(tpi[j]->px(),tpi[j]->py(),tpi[j]->pz(),epi);
          lvp.SetPxPyPzE(tp[k]->px(),tp[k]->py(),tp[k]->pz(),ep);
          lambdac = lvk+lvpi+lvp;
          if(lambdac.Pt() < 5.) continue;
          if(lambdac.M() < 2.1 || lambdac.M() > 2.4) continue;
          // Adding lepton from semi-leptonic b decay
          std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
          for(unsigned int l=0; l<tl.size(); l++) {
            if(tk[i] == tl[l]) continue;
            if(tpi[j] == tl[l]) continue;
            if(tp[k] == tl[l]) continue;
            if(tk[i]->charge()*tl[l]->charge() < 0) continue;
            if(abs(lepton_lambda.at(igen)->pdgId()) == 11) ml = me;
            else if(abs(lepton_lambda.at(igen)->pdgId()) == 13) ml = mmu;
            double el = sqrt(tl[l]->px()*tl[l]->px()+
                             tl[l]->py()*tl[l]->py()+
                             tl[l]->pz()*tl[l]->pz()+ml*ml);
            LV lvl, lambdab;
            lvl.SetPxPyPzE(tl[l]->px(),tl[l]->py(),tl[l]->pz(),el);
            lambdab = lvl+lambdac;
            if(lambdab.M() < 2.4 || lambdab.M() > 6.0) continue;
            nlepton += 1;
            px_lep.push_back(lvl.Px());
            py_lep.push_back(lvl.Py());
            pz_lep.push_back(lvl.Pz());
            pt_lep.push_back(lvl.Pt());
            eta_lep.push_back(lvl.Eta());
            phi_lep.push_back(lvl.Phi());
            mass_lep.push_back(lvl.M());
            energy_lep.push_back(lvl.E());
            charge_lep.push_back(tl[l]->charge());
          }
          if(nlepton < 1) continue;
          v_px_LambdacPlus_lepton_.push_back(px_lep);
          v_py_LambdacPlus_lepton_.push_back(py_lep);
          v_pz_LambdacPlus_lepton_.push_back(pz_lep);
          v_pt_LambdacPlus_lepton_.push_back(pt_lep);
          v_eta_LambdacPlus_lepton_.push_back(eta_lep);
          v_phi_LambdacPlus_lepton_.push_back(phi_lep);
          v_mass_LambdacPlus_lepton_.push_back(mass_lep);
          v_energy_LambdacPlus_lepton_.push_back(energy_lep);
          v_charge_LambdacPlus_lepton_.push_back(charge_lep);
          p4_LambdacPlus_.push_back(lambdac);
          p4_LambdacPlus_kaon_.push_back(lvk);
          p4_LambdacPlus_pion_.push_back(lvpi);
          p4_LambdacPlus_proton_.push_back(lvp);
          v_charge_LambdacPlus_kaon_.push_back(tk[i]->charge());
          v_charge_LambdacPlus_pion_.push_back(tpi[j]->charge());
          v_charge_LambdacPlus_proton_.push_back(tp[k]->charge());
          para.clear(); covmat.clear();
          v_trkpara_LambdacPlus_kaon_ = unpackTrackParameters(kaon_lambda.at(igen),nulltrack,"kaon_lambda");
          v_trkpara_LambdacPlus_pion_ = unpackTrackParameters(pion_lambda.at(igen),nulltrack,"pion_lambda");
          v_trkpara_LambdacPlus_proton_ = unpackTrackParameters(proton_lambda.at(igen),nulltrack,"proton_lambda");
          v_trkcovm_LambdacPlus_kaon_ = unpackTrackCovariance(tk[i],nulltrack,"kaon_lambda");
          v_trkcovm_LambdacPlus_pion_ = unpackTrackCovariance(tpi[j],nulltrack,"pion_lambda");
          v_trkcovm_LambdacPlus_proton_ = unpackTrackCovariance(tp[k],nulltrack,"proton_lambda");
          double pv_lxy = (_pv->x()*lambdac.Px()+
                           _pv->y()*lambdac.Py())/lambdac.Pt();
          v_pv_lxy_LambdacPlus_.push_back(pv_lxy);
        }
      }
    }
  }
}

void TopAnalysis::DataAnalysis(const edm::Event& iEvent,
                               const edm::EventSetup& iSetup,
                               edm::Handle<edm::View<reco::Muon> > &handle1,
                               edm::Handle<edm::View<reco::GsfElectron> > &handle2) {
  edm::Handle<edm::View<reco::TrackJet> > jetsHandle_;
  iEvent.getByToken(jetsToken_,jetsHandle_);
  //cout<<"Jet collection size = "<<jetsHandle_->size()<<endl;
  //cout<<"  Primary vertex pointer = "<<_pv<<endl;
  if(!mumu && !ee && !emu && !mue) return;
  if(zee || zmm) return;
  int ij = 0;
  for(auto iJet = jetsHandle_->begin(); iJet != jetsHandle_->end(); iJet++) {
    if(iJet->pt() < 20.) continue;
    if(fabs(iJet->eta() > 2.4)) continue;
    if(&(*iJet->primaryVertex()) != _pv) continue;
    if(ij == 0) {
      nTrackJet1_ = iJet->numberOfTracks();
    }
    if(ij == 1) {
      nTrackJet2_ = iJet->numberOfTracks();
    }
    if(ij < 2) {
      //cout<<"  jet = "<<ij<<endl;
      std::vector<edm::Ptr<reco::Track> > tracks = iJet->tracks();
      edm::Ptr<reco::Track> track1, track2;
      for(unsigned i=0; i<tracks.size(); i++) {
        if(tracks[i]->pt() < 1.) continue;
        if(fabs(tracks[i]->eta()) > 2.5) continue;
        if(tracks[i]->hitPattern().numberOfValidPixelHits() == 0) continue;
        if(!tracks[i]->quality(reco::TrackBase::highPurity)) continue;
        for(unsigned int j=0; j<tracks.size(); j++) {
          if(tracks[j]->pt() < 1.) continue;
          if(fabs(tracks[j]->eta()) > 2.5) continue;
          if(tracks[j]->hitPattern().numberOfValidPixelHits() == 0) continue;
          if(!tracks[j]->quality(reco::TrackBase::highPurity)) continue;
          for(unsigned int k=0; k<j; k++) {
            nlepton = 0;
            if(i == j) continue;
            if(i == k) continue;
            if(j == k) continue;
            if(tracks[k]->pt() < 1.) continue;
            if(fabs(tracks[k]->eta()) > 2.5) continue;
            if(tracks[k]->hitPattern().numberOfValidPixelHits() == 0) continue;
            if(!tracks[k]->quality(reco::TrackBase::highPurity)) continue;
            if(tracks[i]->charge()*tracks[j]->charge() > 0) continue;
            if(tracks[i]->charge()*tracks[k]->charge() > 0) continue;
            double ek = sqrt(tracks[i]->px()*tracks[i]->px()+
                             tracks[i]->py()*tracks[i]->py()+
                             tracks[i]->pz()*tracks[i]->pz()+mk*mk);
            double epi1 = sqrt(tracks[j]->px()*tracks[j]->px()+
                               tracks[j]->py()*tracks[j]->py()+
                               tracks[j]->pz()*tracks[j]->pz()+mpi*mpi);
            double epi2 = sqrt(tracks[k]->px()*tracks[k]->px()+
                               tracks[k]->py()*tracks[k]->py()+
                               tracks[k]->pz()*tracks[k]->pz()+mpi*mpi);
            LV lvk, lvpi1, lvpi2, dp;
            lvk.SetPxPyPzE(tracks[i]->px(),tracks[i]->py(),tracks[i]->pz(),ek);
            lvpi1.SetPxPyPzE(tracks[j]->px(),tracks[j]->py(),tracks[j]->pz(),epi1);
            lvpi2.SetPxPyPzE(tracks[k]->px(),tracks[k]->py(),tracks[k]->pz(),epi2);
            dp = lvk+lvpi1+lvpi2;
            if(dp.Pt() < 5.) continue;
            if(dp.M() < 1.7 || dp.M() > 2.0) continue;
            // Adding lepton from semi-leptonic B decay
            std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
            for(unsigned int l=0; l<tracks.size(); l++) {
              if(i == l) continue;
              if(j == l) continue;
              if(k == l) continue;
              if(tracks[l]->pt() < 3.) continue;
              if(fabs(tracks[l]->eta()) > 2.5) continue;
              if(tracks[l]->hitPattern().numberOfValidPixelHits() == 0) continue;
              if(!tracks[l]->quality(reco::TrackBase::highPurity)) continue;
              //if(tracks[i]->charge()*tracks[l]->charge() < 0) continue;
              track1 = matchingLepton(tracks[l],handle1);
              track2 = matchingLepton(tracks[l],handle2);
              if(track1 == nulltrack && track2 == nulltrack) continue;
              LV lvl, b0;
              if(track1 != nulltrack) {
                double emu = sqrt(track1->px()*track1->px()+
                                  track1->py()*track1->py()+
                                  track1->pz()*track1->pz()+mmu*mmu);
                lvl.SetPxPyPzE(track1->px(),track1->py(),track1->pz(),emu);
              }
              else if(track2 != nulltrack) {
                double ee = sqrt(track2->px()*track2->px()+
                                 track2->py()*track2->py()+
                                 track2->pz()*track2->pz()+me*me);
                lvl.SetPxPyPzE(track2->px(),track2->py(),track2->pz(),ee);
              }
              b0 = lvl+dp;
              if(b0.M() < 2.0 || b0.M() > 6.0) continue;
              nlepton += 1;
              px_lep.push_back(lvl.Px());
              py_lep.push_back(lvl.Py());
              pz_lep.push_back(lvl.Pz());
              pt_lep.push_back(lvl.Pt());
              eta_lep.push_back(lvl.Eta());
              phi_lep.push_back(lvl.Phi());
              mass_lep.push_back(lvl.M());
              energy_lep.push_back(lvl.E());
              if(track1 != nulltrack) charge_lep.push_back(track1->charge());
              else if(track2 != nulltrack) charge_lep.push_back(track2->charge());
            }
            if(nlepton < 1) continue;
            v_px_DPlus_lepton_.push_back(px_lep);
            v_py_DPlus_lepton_.push_back(py_lep);
            v_pz_DPlus_lepton_.push_back(pz_lep);
            v_pt_DPlus_lepton_.push_back(pt_lep);
            v_eta_DPlus_lepton_.push_back(eta_lep);
            v_phi_DPlus_lepton_.push_back(phi_lep);
            v_mass_DPlus_lepton_.push_back(mass_lep);
            v_energy_DPlus_lepton_.push_back(energy_lep);
            v_charge_DPlus_lepton_.push_back(charge_lep);
            p4_DPlus_.push_back(dp);
            p4_DPlus_kaon_.push_back(lvk);
            p4_DPlus_pion1_.push_back(lvpi1);
            p4_DPlus_pion2_.push_back(lvpi2);
            v_charge_DPlus_kaon_.push_back(tracks[i]->charge());
            v_charge_DPlus_pion1_.push_back(tracks[j]->charge());
            v_charge_DPlus_pion2_.push_back(tracks[k]->charge());
            para.clear(); covmat.clear();
            v_trkpara_DPlus_kaon_ = unpackTrackParameters(NULL,tracks[i],"kaon_dp");
            v_trkpara_DPlus_pion1_ = unpackTrackParameters(NULL,tracks[j],"pion1_dp");
            v_trkpara_DPlus_pion2_ = unpackTrackParameters(NULL,tracks[k],"pion2_dp");
            v_trkcovm_DPlus_kaon_ = unpackTrackCovariance(NULL,tracks[i],"kaon_dp");
            v_trkcovm_DPlus_pion1_ = unpackTrackCovariance(NULL,tracks[j],"pion1_dp");
            v_trkcovm_DPlus_pion2_ = unpackTrackCovariance(NULL,tracks[k],"pion2_dp");
            double pv_lxy = (_pv->x()*dp.Px()+
                             _pv->y()*dp.Py())/dp.Pt();
            v_pv_lxy_DPlus_.push_back(pv_lxy);
          }
        }
      }
      for(unsigned i=0; i<tracks.size(); i++) {
        if(tracks[i]->pt() < 1.) continue;
        if(fabs(tracks[i]->eta()) > 2.5) continue;
        if(tracks[i]->hitPattern().numberOfValidPixelHits() == 0) continue;
        for(unsigned int j=0; j<tracks.size(); j++) {
          nlepton = 0;
          if(i == j) continue;
          if(tracks[j]->pt() < 1.) continue;
          if(fabs(tracks[j]->eta()) > 2.5) continue;
          if(tracks[j]->hitPattern().numberOfValidPixelHits() == 0) continue;
          if(tracks[i]->charge()*tracks[j]->charge() > 0) continue;
          double ek = sqrt(tracks[i]->px()*tracks[i]->px()+
                           tracks[i]->py()*tracks[i]->py()+
                           tracks[i]->pz()*tracks[i]->pz()+mk*mk);
          double epi = sqrt(tracks[j]->px()*tracks[j]->px()+
                            tracks[j]->py()*tracks[j]->py()+
                            tracks[j]->pz()*tracks[j]->pz()+mpi*mpi);
          LV lvk, lvpi, d0;
          lvk.SetPxPyPzE(tracks[i]->px(),tracks[i]->py(),tracks[i]->pz(),ek);
          lvpi.SetPxPyPzE(tracks[j]->px(),tracks[j]->py(),tracks[j]->pz(),epi);
          d0 = lvk+lvpi;
          if(d0.Pt() < 5.) continue;
          if(d0.M() < 1.7 || d0.M() > 2.0) continue;
          // Adding lepton from semi-leptonic b decay
          std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
          for(unsigned int l=0; l<tracks.size(); l++) {
            if(i == l) continue;
            if(j == l) continue;
            if(tracks[l]->pt() < 3.) continue;
            if(fabs(tracks[l]->eta()) > 2.5) continue;
            if(tracks[l]->hitPattern().numberOfValidPixelHits() == 0) continue;
            if(!tracks[l]->quality(reco::TrackBase::highPurity)) continue;
            //if(tracks[i]->charge()*tracks[l]->charge() < 0) continue;
            track1 = matchingLepton(tracks[l],handle1);
            track2 = matchingLepton(tracks[l],handle2);
            if(track1 == nulltrack && track2 == nulltrack) continue;
            LV lvl, bp;
            if(track1 != nulltrack) {
              double emu = sqrt(track1->px()*track1->px()+
                                track1->py()*track1->py()+
                                track1->pz()*track1->pz()+mmu*mmu);
              lvl.SetPxPyPzE(track1->px(),track1->py(),track1->pz(),emu);
            }
            else if(track2 != nulltrack) {
              double ee = sqrt(track2->px()*track2->px()+
                               track2->py()*track2->py()+
                               track2->pz()*track2->pz()+me*me);
              lvl.SetPxPyPzE(track2->px(),track2->py(),track2->pz(),ee);
            }
            bp = lvl+d0;
            if(bp.M() < 2.0 || bp.M() > 6.0) continue;
            nlepton += 1;
            px_lep.push_back(lvl.Px());
            py_lep.push_back(lvl.Py());
            pz_lep.push_back(lvl.Pz());
            pt_lep.push_back(lvl.Pt());
            eta_lep.push_back(lvl.Eta());
            phi_lep.push_back(lvl.Phi());
            mass_lep.push_back(lvl.M());
            energy_lep.push_back(lvl.E());
            if(track1 != nulltrack) charge_lep.push_back(track1->charge());
            else if(track2 != nulltrack) charge_lep.push_back(track2->charge());
          }
          if(nlepton < 1) continue;
          v_px_D0_lepton_.push_back(px_lep);
          v_py_D0_lepton_.push_back(py_lep);
          v_pz_D0_lepton_.push_back(pz_lep);
          v_pt_D0_lepton_.push_back(pt_lep);
          v_eta_D0_lepton_.push_back(eta_lep);
          v_phi_D0_lepton_.push_back(phi_lep);
          v_mass_D0_lepton_.push_back(mass_lep);
          v_energy_D0_lepton_.push_back(energy_lep);
          v_charge_D0_lepton_.push_back(charge_lep);
          p4_D0_.push_back(d0);
          p4_D0_kaon_.push_back(lvk);
          p4_D0_pion_.push_back(lvpi);
          v_charge_D0_kaon_.push_back(tracks[i]->charge());
          v_charge_D0_pion_.push_back(tracks[j]->charge());
          para.clear(); covmat.clear();
          v_trkpara_D0_kaon_ = unpackTrackParameters(NULL,tracks[i],"kaon_d0");
          v_trkpara_D0_pion_ = unpackTrackParameters(NULL,tracks[j],"pion_d0");
          v_trkcovm_D0_kaon_ = unpackTrackCovariance(NULL,tracks[i],"kaon_d0");
          v_trkcovm_D0_pion_ = unpackTrackCovariance(NULL,tracks[j],"pion_d0");
          double pv_lxy = (_pv->x()*d0.Px()+
                           _pv->y()*d0.Py())/d0.Pt();
          v_pv_lxy_D0_.push_back(pv_lxy);
        }
      }
      for(unsigned i=0; i<tracks.size(); i++) {
        if(tracks[i]->pt() < 1.) continue;
        if(fabs(tracks[i]->eta()) > 2.5) continue;
        if(tracks[i]->hitPattern().numberOfValidPixelHits() == 0) continue;
        for(unsigned int j=0; j<tracks.size(); j++) {
          if(tracks[j]->pt() < 1.) continue;
          if(fabs(tracks[j]->eta()) > 2.5) continue;
          if(tracks[j]->hitPattern().numberOfValidPixelHits() == 0) continue;
          for(unsigned int k=0; k<tracks.size(); k++) {
            nlepton = 0;
            if(i == j) continue;
            if(i == k) continue;
            if(j == k) continue;
            if(tracks[k]->pt() < 1.) continue;
            if(fabs(tracks[k]->eta()) > 2.5) continue;
            if(tracks[k]->hitPattern().numberOfValidPixelHits() == 0) continue;
            if(tracks[i]->charge()*tracks[j]->charge() > 0) continue;
            if(tracks[i]->charge()*tracks[k]->charge() > 0) continue;
            double ek = sqrt(tracks[i]->px()*tracks[i]->px()+
                             tracks[i]->py()*tracks[i]->py()+
                             tracks[i]->pz()*tracks[i]->pz()+mk*mk);
            double epi = sqrt(tracks[j]->px()*tracks[j]->px()+
                              tracks[j]->py()*tracks[j]->py()+
                              tracks[j]->pz()*tracks[j]->pz()+mpi*mpi);
            double epis = sqrt(tracks[k]->px()*tracks[k]->px()+
                              tracks[k]->py()*tracks[k]->py()+
                              tracks[k]->pz()*tracks[k]->pz()+mpi*mpi);
            LV lvk, lvpi, lvpis, d0, dt;
            lvk.SetPxPyPzE(tracks[i]->px(),tracks[i]->py(),tracks[i]->pz(),ek);
            lvpi.SetPxPyPzE(tracks[j]->px(),tracks[j]->py(),tracks[j]->pz(),epi);
            lvpis.SetPxPyPzE(tracks[k]->px(),tracks[k]->py(),tracks[k]->pz(),epis);
            d0 = lvk+lvpi;
            dt = lvk+lvpi+lvpis;
            if(d0.Pt() < 5.) continue;
            if(d0.M() < 1.7 || d0.M() > 2.0) continue;
            // Adding lepton from semi-leptonic b decay
            std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
            for(unsigned int l=0; l<tracks.size(); l++) {
              if(i == l) continue;
              if(j == l) continue;
              if(k == l) continue;
              if(tracks[l]->pt() < 3.) continue;
              if(fabs(tracks[l]->eta()) > 2.5) continue;
              if(tracks[l]->hitPattern().numberOfValidPixelHits() == 0) continue;
              if(!tracks[l]->quality(reco::TrackBase::highPurity)) continue;
              //if(tracks[i]->charge()*tracks[l]->charge() < 0) continue;
              track1 = matchingLepton(tracks[l],handle1);
              track2 = matchingLepton(tracks[l],handle2);
              if(track1 == nulltrack && track2 == nulltrack) continue;
              LV lvl, b0;
              if(track1 != nulltrack) {
                double emu = sqrt(track1->px()*track1->px()+
                                  track1->py()*track1->py()+
                                  track1->pz()*track1->pz()+mmu*mmu);
                lvl.SetPxPyPzE(track1->px(),track1->py(),track1->pz(),emu);
              }
              else if(track2 != nulltrack) {
                double ee = sqrt(track2->px()*track2->px()+
                                 track2->py()*track2->py()+
                                 track2->pz()*track2->pz()+me*me);
                lvl.SetPxPyPzE(track2->px(),track2->py(),track2->pz(),ee);
              }
              b0 = lvl+dt;
              if(b0.M() < 2.0 || b0.M() > 6.0) continue;
              nlepton += 1;
              px_lep.push_back(lvl.Px());
              py_lep.push_back(lvl.Py());
              pz_lep.push_back(lvl.Pz());
              pt_lep.push_back(lvl.Pt());
              eta_lep.push_back(lvl.Eta());
              phi_lep.push_back(lvl.Phi());
              mass_lep.push_back(lvl.M());
              energy_lep.push_back(lvl.E());
              if(track1 != nulltrack) charge_lep.push_back(track1->charge());
              else if(track2 != nulltrack) charge_lep.push_back(track2->charge());
            }
            if(nlepton < 1) continue;
            v_px_Dstar_lepton_.push_back(px_lep);
            v_py_Dstar_lepton_.push_back(py_lep);
            v_pz_Dstar_lepton_.push_back(pz_lep);
            v_pt_Dstar_lepton_.push_back(pt_lep);
            v_eta_Dstar_lepton_.push_back(eta_lep);
            v_phi_Dstar_lepton_.push_back(phi_lep);
            v_mass_Dstar_lepton_.push_back(mass_lep);
            v_energy_Dstar_lepton_.push_back(energy_lep);
            v_charge_Dstar_lepton_.push_back(charge_lep);
            mdiff_Dstar_D0_.push_back(dt.M()-d0.M());
            p4_Dstar_.push_back(dt);
            p4_Dstar_D0_.push_back(d0);
            p4_Dstar_D0_kaon_.push_back(lvk);
            p4_Dstar_D0_pion_.push_back(lvpi);
            p4_Dstar_pionsoft_.push_back(lvpis);
            v_charge_Dstar_D0_kaon_.push_back(tracks[i]->charge());
            v_charge_Dstar_D0_pion_.push_back(tracks[j]->charge());
            v_charge_Dstar_pionsoft_.push_back(tracks[k]->charge());
            para.clear(); covmat.clear();
            v_trkpara_Dstar_D0_kaon_ = unpackTrackParameters(NULL,tracks[i],"kaon_dt");
            v_trkpara_Dstar_D0_pion_ = unpackTrackParameters(NULL,tracks[j],"pion_dt");
            v_trkpara_Dstar_pionsoft_ = unpackTrackParameters(NULL,tracks[k],"pionsoft_dt");
            v_trkcovm_Dstar_D0_kaon_ = unpackTrackCovariance(NULL,tracks[i],"kaon_dt");
            v_trkcovm_Dstar_D0_pion_ = unpackTrackCovariance(NULL,tracks[j],"pion_dt");
            v_trkcovm_Dstar_pionsoft_ = unpackTrackCovariance(NULL,tracks[k],"pionsoft_dt");
            double pv_lxy = (_pv->x()*d0.Px()+
                             _pv->y()*d0.Py())/d0.Pt();
            v_pv_lxy_Dstar_D0_.push_back(pv_lxy);
          }
        }
      }
      for(unsigned i=0; i<tracks.size(); i++) {
        if(tracks[i]->pt() < 1.) continue;
        if(fabs(tracks[i]->eta()) > 2.5) continue;
        if(tracks[i]->hitPattern().numberOfValidPixelHits() == 0) continue;
        for(unsigned int j=0; j<i; j++) {
          if(tracks[j]->pt() < 1.) continue;
          if(fabs(tracks[j]->eta()) > 2.5) continue;
          if(tracks[j]->hitPattern().numberOfValidPixelHits() == 0) continue;
          for(unsigned int k=0; k<tracks.size(); k++) {
            nlepton = 0;
            if(i == j) continue;
            if(i == k) continue;
            if(j == k) continue;
            if(tracks[k]->pt() < 1.) continue;
            if(fabs(tracks[k]->eta()) > 2.5) continue;
            if(tracks[k]->hitPattern().numberOfValidPixelHits() == 0) continue;
            if(tracks[i]->charge()*tracks[j]->charge() > 0) continue;
            double ek1 = sqrt(tracks[i]->px()*tracks[i]->px()+
                              tracks[i]->py()*tracks[i]->py()+
                              tracks[i]->pz()*tracks[i]->pz()+mk*mk);
            double ek2 = sqrt(tracks[j]->px()*tracks[j]->px()+
                              tracks[j]->py()*tracks[j]->py()+
                              tracks[j]->pz()*tracks[j]->pz()+mk*mk);
            double epi = sqrt(tracks[k]->px()*tracks[k]->px()+
                              tracks[k]->py()*tracks[k]->py()+
                              tracks[k]->pz()*tracks[k]->pz()+mpi*mpi);
            LV lvk1, lvk2, lvpi, ds;
            lvk1.SetPxPyPzE(tracks[i]->px(),tracks[i]->py(),tracks[i]->pz(),ek1);
            lvk2.SetPxPyPzE(tracks[j]->px(),tracks[j]->py(),tracks[j]->pz(),ek2);
            lvpi.SetPxPyPzE(tracks[k]->px(),tracks[k]->py(),tracks[k]->pz(),epi);
            ds = lvk1+lvk2+lvpi;
            if(ds.Pt() < 5.) continue;
            if(ds.M() < 1.8 || ds.M() > 2.1) continue;
            // Adding lepton from semi-leptonic b decay
            std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep, mass_lep, energy_lep, charge_lep;
            for(unsigned int l=0; l<tracks.size(); l++) {
              if(i == l) continue;
              if(j == l) continue;
              if(k == l) continue;
              if(tracks[l]->pt() < 3.) continue;
              if(fabs(tracks[l]->eta()) > 2.5) continue;
              if(tracks[l]->hitPattern().numberOfValidPixelHits() == 0) continue;
              if(!tracks[l]->quality(reco::TrackBase::highPurity)) continue;
              //if(tracks[k]->charge()*tracks[l]->charge() > 0) continue;
              track1 = matchingLepton(tracks[l],handle1);
              track2 = matchingLepton(tracks[l],handle2);
              if(track1 == nulltrack && track2 == nulltrack) continue;
              LV lvl, bs;
              if(track1 != nulltrack) {
                double emu = sqrt(track1->px()*track1->px()+
                                  track1->py()*track1->py()+
                                  track1->pz()*track1->pz()+mmu*mmu);
                lvl.SetPxPyPzE(track1->px(),track1->py(),track1->pz(),emu);
              }
              else if(track2 != nulltrack) {
                double ee = sqrt(track2->px()*track2->px()+
                                 track2->py()*track2->py()+
                                 track2->pz()*track2->pz()+me*me);
                lvl.SetPxPyPzE(track2->px(),track2->py(),track2->pz(),ee);
              }
              bs = lvl+ds;
              if(bs.M() < 2.1 || bs.M() > 6.0) continue;
              nlepton += 1;
              px_lep.push_back(lvl.Px());
              py_lep.push_back(lvl.Py());
              pz_lep.push_back(lvl.Pz());
              pt_lep.push_back(lvl.Pt());
              eta_lep.push_back(lvl.Eta());
              phi_lep.push_back(lvl.Phi());
              mass_lep.push_back(lvl.M());
              energy_lep.push_back(lvl.E());
              if(track1 != nulltrack) charge_lep.push_back(track1->charge());
              else if(track2 != nulltrack) charge_lep.push_back(track2->charge());
            }
            if(nlepton < 1) continue;
            v_px_DsPlus_lepton_.push_back(px_lep);
            v_py_DsPlus_lepton_.push_back(py_lep);
            v_pz_DsPlus_lepton_.push_back(pz_lep);
            v_pt_DsPlus_lepton_.push_back(pt_lep);
            v_eta_DsPlus_lepton_.push_back(eta_lep);
            v_phi_DsPlus_lepton_.push_back(phi_lep);
            v_mass_DsPlus_lepton_.push_back(mass_lep);
            v_energy_DsPlus_lepton_.push_back(energy_lep);
            v_charge_DsPlus_lepton_.push_back(charge_lep);
            p4_DsPlus_.push_back(ds);
            p4_DsPlus_kaon1_.push_back(lvk1);
            p4_DsPlus_kaon2_.push_back(lvk2);
            p4_DsPlus_pion_.push_back(lvpi);
            v_charge_DsPlus_kaon1_.push_back(tracks[i]->charge());
            v_charge_DsPlus_kaon2_.push_back(tracks[j]->charge());
            v_charge_DsPlus_pion_.push_back(tracks[k]->charge());
            para.clear(); covmat.clear();
            v_trkpara_DsPlus_kaon1_ = unpackTrackParameters(NULL,tracks[i],"kaon1_ds");
            v_trkpara_DsPlus_kaon2_ = unpackTrackParameters(NULL,tracks[j],"kaon2_ds");
            v_trkpara_DsPlus_pion_ = unpackTrackParameters(NULL,tracks[k],"pion_ds");
            v_trkcovm_DsPlus_kaon1_ = unpackTrackCovariance(NULL,tracks[i],"kaon1_ds");
            v_trkcovm_DsPlus_kaon2_ = unpackTrackCovariance(NULL,tracks[j],"kaon2_ds");
            v_trkcovm_DsPlus_pion_ = unpackTrackCovariance(NULL,tracks[k],"pion_ds");
            double pv_lxy = (_pv->x()*ds.Px()+
                             _pv->y()*ds.Py())/ds.Pt();
            v_pv_lxy_DsPlus_.push_back(pv_lxy);
          }
        }
      }
      for(unsigned i=0; i<tracks.size(); i++) {
        if(tracks[i]->pt() < 1.) continue;
        if(fabs(tracks[i]->eta()) > 2.5) continue;
        if(tracks[i]->hitPattern().numberOfValidPixelHits() == 0) continue;
        for(unsigned int j=0; j<tracks.size(); j++) {
          if(tracks[j]->pt() < 1.) continue;
          if(fabs(tracks[j]->eta()) > 2.5) continue;
          if(tracks[j]->hitPattern().numberOfValidPixelHits() == 0) continue;
          for(unsigned int k=0; k<tracks.size(); k++) {
            nlepton = 0;
            if(i == j) continue;
            if(i == k) continue;
            if(j == k) continue;
            if(tracks[k]->pt() < 1.) continue;
            if(fabs(tracks[k]->eta()) > 2.5) continue;
            if(tracks[k]->hitPattern().numberOfValidPixelHits() == 0) continue;
            if(tracks[i]->charge()*tracks[j]->charge() > 0) continue;
            if(tracks[i]->charge()*tracks[k]->charge() > 0) continue;
            double ek = sqrt(tracks[i]->px()*tracks[i]->px()+
                             tracks[i]->py()*tracks[i]->py()+
                             tracks[i]->pz()*tracks[i]->pz()+mk*mk);
            double epi = sqrt(tracks[j]->px()*tracks[j]->px()+
                              tracks[j]->py()*tracks[j]->py()+
                              tracks[j]->pz()*tracks[j]->pz()+mpi*mpi);
            double ep = sqrt(tracks[k]->px()*tracks[k]->px()+
                             tracks[k]->py()*tracks[k]->py()+
                             tracks[k]->pz()*tracks[k]->pz()+mp*mp);
            LV lvk, lvpi, lvp, lambdac;
            lvk.SetPxPyPzE(tracks[i]->px(),tracks[i]->py(),tracks[i]->pz(),ek);
            lvpi.SetPxPyPzE(tracks[j]->px(),tracks[j]->py(),tracks[j]->pz(),epi);
            lvp.SetPxPyPzE(tracks[k]->px(),tracks[k]->py(),tracks[k]->pz(),ep);
            lambdac = lvk+lvpi+lvp;
            if(lambdac.Pt() < 5.) continue;
            if(lambdac.M() < 2.1 || lambdac.M() > 2.4) continue;
            // Adding lepton from semi-leptonic b decay
            std::vector<double> px_lep, py_lep, pz_lep, pt_lep, eta_lep, phi_lep,
                                mass_lep, energy_lep, charge_lep, cos_theta;
            for(unsigned int l=0; l<tracks.size(); l++) {
              if(i == l) continue;
              if(j == l) continue;
              if(k == l) continue;
              if(tracks[l]->pt() < 3.) continue;
              if(fabs(tracks[l]->eta()) > 2.5) continue;
              if(tracks[l]->hitPattern().numberOfValidPixelHits() == 0) continue;
              if(!tracks[l]->quality(reco::TrackBase::highPurity)) continue;
              //if(tracks[i]->charge()*tracks[l]->charge() < 0) continue;
              track1 = matchingLepton(tracks[l],handle1);
              track2 = matchingLepton(tracks[l],handle2);
              if(track1 == nulltrack && track2 == nulltrack) continue;
              LV lvl, lambdab;
              if(track1 != nulltrack) {
                double emu = sqrt(track1->px()*track1->px()+
                                  track1->py()*track1->py()+
                                  track1->pz()*track1->pz()+mmu*mmu);
                lvl.SetPxPyPzE(track1->px(),track1->py(),track1->pz(),emu);
              }
              else if(track2 != nulltrack) {
                double ee = sqrt(track2->px()*track2->px()+
                                 track2->py()*track2->py()+
                                 track2->pz()*track2->pz()+me*me);
                lvl.SetPxPyPzE(track2->px(),track2->py(),track2->pz(),ee);
              }
              lambdab = lvl+lambdac;
              if(lambdab.M() < 2.4 || lambdab.M() > 6.0) continue;
              nlepton += 1;
              px_lep.push_back(lvl.Px());
              py_lep.push_back(lvl.Py());
              pz_lep.push_back(lvl.Pz());
              pt_lep.push_back(lvl.Pt());
              eta_lep.push_back(lvl.Eta());
              phi_lep.push_back(lvl.Phi());
              mass_lep.push_back(lvl.M());
              energy_lep.push_back(lvl.E());
              if(track1 != nulltrack) charge_lep.push_back(track1->charge());
              else if(track2 != nulltrack) charge_lep.push_back(track2->charge());
              double num = lambdac.Px()*lvl.Px() + lambdac.Py()*lvl.Py();
              double den = sqrt(lambdac.Px()*lambdac.Px()+lambdac.Py()*lambdac.Py())*
                           sqrt(lvl.Px()*lvl.Px()+lvl.Py()*lvl.Py());
              cos_theta.push_back(TMath::Cos(num/den));
            }
            if(nlepton < 1) continue;
            v_px_LambdacPlus_lepton_.push_back(px_lep);
            v_py_LambdacPlus_lepton_.push_back(py_lep);
            v_pz_LambdacPlus_lepton_.push_back(pz_lep);
            v_pt_LambdacPlus_lepton_.push_back(pt_lep);
            v_eta_LambdacPlus_lepton_.push_back(eta_lep);
            v_phi_LambdacPlus_lepton_.push_back(phi_lep);
            v_mass_LambdacPlus_lepton_.push_back(mass_lep);
            v_energy_LambdacPlus_lepton_.push_back(energy_lep);
            v_charge_LambdacPlus_lepton_.push_back(charge_lep);
            p4_LambdacPlus_.push_back(lambdac);
            p4_LambdacPlus_kaon_.push_back(lvk);
            p4_LambdacPlus_pion_.push_back(lvpi);
            p4_LambdacPlus_proton_.push_back(lvp);
            v_charge_LambdacPlus_kaon_.push_back(tracks[i]->charge());
            v_charge_LambdacPlus_pion_.push_back(tracks[j]->charge());
            v_charge_LambdacPlus_proton_.push_back(tracks[k]->charge());
            v_costheta_LambdacPlus_lepton_.push_back(cos_theta);
            para.clear(); covmat.clear();
            v_trkpara_LambdacPlus_kaon_ = unpackTrackParameters(NULL,tracks[i],"kaon_lambda");
            v_trkpara_LambdacPlus_pion_ = unpackTrackParameters(NULL,tracks[j],"pion_lambda");
            v_trkpara_LambdacPlus_proton_ = unpackTrackParameters(NULL,tracks[k],"proton_lambda");
            v_trkcovm_LambdacPlus_kaon_ = unpackTrackCovariance(NULL,tracks[i],"kaon_lambda");
            v_trkcovm_LambdacPlus_pion_ = unpackTrackCovariance(NULL,tracks[j],"pion_lambda");
            v_trkcovm_LambdacPlus_proton_ = unpackTrackCovariance(NULL,tracks[k],"proton_lambda");
            double pv_lxy = (_pv->x()*lambdac.Px()+
                             _pv->y()*lambdac.Py())/lambdac.Pt();
            v_pv_lxy_LambdacPlus_.push_back(pv_lxy);
          }
        }
      }
    }
    ij += 1;
  }
  nJetPV_ = ij;
}

template<typename TYPE>
LV TopAnalysis::getLV(const TYPE& p4) const {
  return LV(p4.pt(), p4.eta(), p4.phi(), p4.mass());
}

bool TopAnalysis::isTopAncestor(const reco::Candidate* particle) { 
  if(abs(particle->pdgId()) == 6 &&
         particle->status() == 62) {
    top_quarks.push_back(particle);       
    return true;
  }
  for(size_t i=0; i<particle->numberOfMothers(); i++) {
    if(isTopAncestor(particle->mother(i))) {
      return true;
    }
  }
  return false;
}

//bool TopAnalysis::isTight(const edm::Ptr<reco::Muon>& muon) {
bool TopAnalysis::isMuon(const reco::Muon* muon) {
  if(muon->isGlobalMuon() &&
     muon->isPFMuon() &&
     muon->globalTrack()->normalizedChi2() < 10. &&
     muon->globalTrack()->hitPattern().numberOfValidMuonHits() > 0 &&
     muon->numberOfMatchedStations() > 1 &&
     fabs(muon->muonBestTrack()->dxy(_pv->position())) < 0.2 &&
     fabs(muon->muonBestTrack()->dz(_pv->position())) < 0.5 &&
     muon->innerTrack()->hitPattern().numberOfValidPixelHits() > 0 &&
     muon->innerTrack()->hitPattern().trackerLayersWithMeasurement() > 5) {
    return true;
  }
  return false;
}

bool TopAnalysis::dRLeptonJet(std::vector<const reco::Muon*> muons,const reco::TrackJet &jets) {
  for(unsigned i=0; i<muons.size(); i++) {
    double deta = muons[i]->eta() - jets.eta();
    double dphi = reco::deltaPhi(muons[i]->phi(),jets.phi());
    double dR = sqrt(deta*deta+dphi*dphi);
    if(dR > 0.4) continue;
    return true;
  }
  return false;
}

std::vector<const reco::Track*> TopAnalysis::matchingTrack(const reco::Candidate* cand, 
                                                           edm::Handle<edm::View<reco::Track> > &handle) {
  std::vector<const reco::Track *> list;
  for(edm::View<reco::Track>::const_iterator i = handle->begin();
                                             i != handle->end(); i++) {
    if(!(i->quality(reco::TrackBase::highPurity)) ||
         i->hitPattern().numberOfValidPixelHits() < 1) continue;
    if(i->pt() < 1.0) continue;
    if(fabs(i->eta()) > 2.5) continue;
    double deta = cand->eta() - i->eta();
    double dphi = reco::deltaPhi(cand->phi(), i->phi());
    double dR = sqrt(deta*deta + dphi*dphi);
    if(fabs(dR) < 0.02) {
      list.push_back(&(*i));
    }
  }
  return list;
}

edm::Ptr<reco::Track> TopAnalysis::matchingLepton(edm::Ptr<reco::Track>& track,
                                                  edm::Handle<edm::View<reco::Muon> > &handle) {
  edm::Ptr<reco::Track> list;
  for(edm::View<reco::Muon>::const_iterator i = handle->begin();
                                            i != handle->end(); i++) {
    double deta = track->eta() - i->eta();
    double dphi = reco::deltaPhi(track->phi(), i->phi());
    double dR = sqrt(deta*deta + dphi*dphi);
    if(fabs(dR) < 0.00001) {
      return track;
    }
  }
  return list;
}

edm::Ptr<reco::Track> TopAnalysis::matchingLepton(edm::Ptr<reco::Track>& track,
                                                  edm::Handle<edm::View<reco::GsfElectron> > &handle) {
  edm::Ptr<reco::Track> list;
  for(edm::View<reco::GsfElectron>::const_iterator i = handle->begin();
                                                   i != handle->end(); i++) {
    double deta = track->eta() - i->eta();
    double dphi = reco::deltaPhi(track->phi(), i->phi());
    double dR = sqrt(deta*deta + dphi*dphi);
    if(fabs(dR) < 0.00001) {
      return track;
    }
  }
  return list;
}

std::vector<std::vector<double>> TopAnalysis::unpackTrackParameters(const reco::Candidate* cand,
                                 edm::Ptr<reco::Track>& track,
                                 std::string name) {
  para.clear();
  double curvature, lambda, phi, dxy, dsz;
  if(cand != nullptr) {
    curvature = cand->charge()/cand->p();
    lambda = Pi()/2 - cand->theta();
    phi = cand->phi();
    dxy = -cand->vx()*Sin(cand->phi()) + cand->vy()*Cos(cand->phi());
    dsz = cand->vz()*Cos(lambda) - (cand->vx()*Cos(cand->phi()) + cand->vy()*Sin(cand->phi()))*Sin(lambda);
  }
  if(& *track != nullptr) {
    curvature = track->charge()/track->p();
    lambda = Pi()/2 - track->theta();
    phi = track->phi();
    dxy = -track->vx()*Sin(track->phi()) + track->vy()*Cos(track->phi());
    dsz = track->vz()*Cos(lambda) - (track->vx()*Cos(track->phi()) + track->vy()*Sin(track->phi()))*Sin(lambda);
  }
  //cout<<"    "<<name<<" parameters: "<<curvature<<"  "<<lambda<<"  "<<phi<<"  "<<dxy<<"  "<<dsz<<endl;
  para.push_back(curvature); para.push_back(lambda); para.push_back(phi); para.push_back(dxy); para.push_back(dsz);
  if(name=="kaon_dp") { kdp_par.push_back(para); return kdp_par; }
  if(name=="pion1_dp") { pi1dp_par.push_back(para); return pi1dp_par; }
  if(name=="pion2_dp") { pi2dp_par.push_back(para); return pi2dp_par; }
  if(name=="kaon_d0") { kd0_par.push_back(para); return kd0_par; }
  if(name=="pion_d0") { pid0_par.push_back(para); return pid0_par; }
  if(name=="kaon_dt") { kdt_par.push_back(para); return kdt_par; }
  if(name=="pion_dt") { pidt_par.push_back(para); return pidt_par; }
  if(name=="pionsoft_dt") { pisdt_par.push_back(para); return pisdt_par; }
  if(name=="kaon1_ds") { k1ds_par.push_back(para); return k1ds_par; }
  if(name=="kaon2_ds") { k2ds_par.push_back(para); return k2ds_par; }
  if(name=="pion_ds") { pids_par.push_back(para); return pids_par; }
  if(name=="kaon_lambda") { klambda_par.push_back(para); return klambda_par; }
  if(name=="pion_lambda") { pilambda_par.push_back(para); return pilambda_par; }
  if(name=="proton_lambda") { plambda_par.push_back(para); return plambda_par; }
  else return par;
}

std::vector<std::vector<double>> TopAnalysis::unpackTrackCovariance(const reco::Track* track1,
                                 edm::Ptr<reco::Track>& track2,
                                 std::string name) {
  covmat.clear();
  reco::TrackBase::CovarianceMatrix cov;
  if(track1 != nullptr) cov = track1->covariance();
  else if(& *track2 != nullptr) cov = track2->covariance();
  for(int i=0; i<cov.kRows; i++)
    for(int j=0; j<cov.kRows; j++)
      covmat.push_back(cov(i,j));
  //cout<<"  covariance matrix: "<<cov<<endl;
  if(name=="kaon_dp") { kdp_cov.push_back(covmat); return kdp_cov; }
  if(name=="pion1_dp") { pi1dp_cov.push_back(covmat); return pi1dp_cov; }
  if(name=="pion2_dp") { pi2dp_cov.push_back(covmat); return pi2dp_cov; }
  if(name=="kaon_d0") { kd0_cov.push_back(covmat); return kd0_cov; }
  if(name=="pion_d0") { pid0_cov.push_back(covmat); return pid0_cov; }
  if(name=="kaon_dt") { kdt_cov.push_back(covmat); return kdt_cov; }
  if(name=="pion_dt") { pidt_cov.push_back(covmat); return pidt_cov; }
  if(name=="pionsoft_dt") { pisdt_cov.push_back(covmat); return pisdt_cov; }
  if(name=="kaon1_ds") { k1ds_cov.push_back(covmat); return k1ds_cov; }
  if(name=="kaon2_ds") { k2ds_cov.push_back(covmat); return k2ds_cov; }
  if(name=="pion_ds") { pids_cov.push_back(covmat); return pids_cov; }
  if(name=="kaon_lambda") { klambda_cov.push_back(covmat); return klambda_cov; }
  if(name=="pion_lambda") { pilambda_cov.push_back(covmat); return pilambda_cov; }
  if(name=="proton_lambda") { plambda_cov.push_back(covmat); return plambda_cov; }
  else return par;
}

// ------------ method called once each job just before starting event loop  ------------
void 
TopAnalysis::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
TopAnalysis::endJob() 
{
  //cout<<"N(K,pi) = "<<nkpi<<endl;
  //cout<<"N(K,pi,pi) = "<<nkpipi<<endl;
  //cout<<"N(K,K,pi) = "<<nkkpi<<endl;
  //cout<<"N(p,K,pi) = "<<npkpi<<endl;
  //cout<<"N(D*+) = "<<ndstar<<endl;
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
TopAnalysis::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(TopAnalysis);
